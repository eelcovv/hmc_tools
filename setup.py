#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Setup file for hmc_tools.

    This file was generated with PyScaffold 3.0.
    PyScaffold helps you to put up the scaffold of your new Python project.
    Learn more under: http://pyscaffold.readthedocs.org/
"""

import sys
from setuptools import setup

# Add here console scripts and other entry points in ini-style format
entry_points = """
[console_scripts]
ffmpeg_movie = hmc_tools.ffmpeg_movie:_run
netcdf_manipulate = hmc_tools.netcdf_manipulate:_run
ftp_to_netcdf = hmc_tools.ftp_to_netcdf:_run
route_interpolate = hmc_tools.route_interpolate:_run
svg_objects_export = hmc_tools.svg_objects_export:_run
"""


def setup_package():
    needs_sphinx = {'build_sphinx', 'upload_docs'}.intersection(sys.argv)
    sphinx = ['sphinx'] if needs_sphinx else []
    setup(setup_requires=['pyscaffold>=3.0a0,<3.1a0'] + sphinx,
          entry_points=entry_points,
          use_pyscaffold=True)


if __name__ == "__main__":
    setup_package()
