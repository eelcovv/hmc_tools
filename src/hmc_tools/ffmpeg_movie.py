#!/bin/sh
"""

This script generated a movie using ffmpeg with globbing allowed (not possible for the current
window version). The usage is :

ffmpeg_movie.py movie_frame_t*.png

this will turn a sequence movie_frame_t0.png .. movie_frame_t100.png into a movie. The numbers may be
both unit spaced 0, 1, 2, ..., 100 or non-unit spaced, like 0, 10, 20, 30, ...,1000. 
Also leading zero's are allowed like 000, 005, 010, 015, ... 100
"""

import argparse
import logging
import os
import re  # regular expressions
import shutil
import subprocess
import sys
from glob import glob

import numpy as np
from PIL import Image
from hmc_utils.misc import (create_logger, get_logger)

from hmc_tools import __version__


class MovieInfo:
    """
    Class to hold the movie information

    Parameters
    ----------
    files: list
        List of file names to be processed
    pattern: str
        Regular expression to select the movie files
    filter_base_name:  str, optional
        Base name of the filter used to clear up the image root. Default is "(\.|_)[t]*$"
        (i.e. ending with an underscore, such as image_name_0000.png, possible with a t, seach as
        image_name_t00000.png, then the last _ before the number  is removed
    test: bool
        Only show the command to be executes, to not do it yet
    force: bool
        Force to create the movi
    fps: int
        Frame per second of the movie
    mode: {ffmpeg, menoder}
        Two different ways to create a movie
    high_quality: bool
        If true, make the highest quality movie possible
    link_base: str, optional
        If the sequence of file number does not have a unit step of one, all file need to be linked
        first to a file link. The nane of this file link is given by the string 'link_base'.
        Default = "link_base"
    frame_multiplication_factor: int
        Add extra frames
    output: str
        Name of the output movie
    movie_type: {"mp4", "avi"}
        Type of the movie generated
    codec: {"msmpeg4v2"}
        Code to be used
    btr: int
        Bit rate of the movie
    mbd: int
    file_list_name: str
        Name of the file containing the list of movie image.
    """

    def __init__(self, files,
                 pattern=None,
                 filter_base_name="(\.|_)[ti]*$",
                 test=False,
                 force=False,
                 fps=10,
                 mode="ffmeg",
                 high_quality=False,
                 frame_multiplication_factor=None,
                 output=None,
                 link_base="image_ling",
                 movie_type=None,
                 codec="msmpeg4v2",
                 btr=None,
                 mbd=2,
                 file_list_name=None):

        self.log = get_logger(__name__)

        self.mode = mode
        self.files = files
        self.pattern = pattern
        self.filter_base_name = filter_base_name

        self.log.info("Set file {}".format(file_list_name))
        self.file_list_name = file_list_name

        self.output = output

        if movie_type is None:
            if self.mode == "ffmpeg":
                self.movie_type = "mp4"
            elif self.mode == "mencoder":
                self.movie_type = "avi"
            else:
                raise ValueError("mode should be 'ffmpeg' or 'mencoder'")
        else:
            self.movie_type = movie_type

        self.test = test
        self.force = force

        self.fps = fps
        self.frame_multiplication_factor = frame_multiplication_factor

        self.codec = codec
        self.btr = btr
        self.bitrate = None
        self.mbd = mbd

        self.high_quality = high_quality
        self.movie_def = None
        self.movie_ext = None

        self.image_file_root = None
        self.image_file_root_bare = None
        self.number_prefix = "."
        self.file_number_list = []
        self.file_list = []
        self.step_size = []
        self.commands = []

        self.link_base = link_base

        self.image_width = None
        self.image_height = None

        self.n_digits = None
        self.n_digit_list = []
        self.leading_zero = True

        self.file_base = None
        self.image_extension = None

        self.image_link_list = []

        self.analyse_input_files()
        self.make_link_list()
        self.make_output_filename()

    def analyse_input_files(self):
        """
        Analyse the input file list
        """

        if self.pattern is not None:
            try:
                reg_exp = re.compile(self.pattern)
            except re.error:
                self.log.warning("failed the regular expression for the number pattern.")
                sys.exit("Goodbye!")
        else:
            reg_exp = None

        # get the file list based on the search pattern and sort alphabetically
        if self.file_list_name is None:
            self.log.debug("glob files {}".format(self.files))
            self.file_list = glob(self.files)
        else:
            # we have passed a list of file names. Read the files
            with open(self.file_list_name, "r") as fp:
                self.file_list = list()
                for file_name in fp.readlines():
                    self.file_list.append(file_name.strip())

        self.file_list.sort()
        self.log.debug("found file list {}".format(self.file_list))

        # first loop in order to analyse the file sequence. See if the number is
        # with step size 1 (is ok for ffmpeg) or something else (not ok). In the
        # latter case, the images need to be copied to force a correct sequence
        for (cnt, file1) in enumerate(self.file_list):
            self.log.debug("analysing file {}".format(file1))

            self.file_base, self.image_extension = os.path.splitext(file1)
            self.image_extension = re.sub("^\.", "", self.image_extension)
            self.log.debug("file_base {} ext {}".format(self.file_base, self.image_extension))

            match = reg_exp.search(self.file_base)
            if bool(match):
                file_number = match.group(1)
                self.log.debug("MATCH {}".format(file_number))

                # count the number of digits of the current file number and store in
                # a list
                self.n_digits = len(file_number)
                self.n_digit_list.append(self.n_digits)

                # if the leading_zero is still true, see if the current number of
                # digits differs from any in the list
                if self.leading_zero and (np.array(self.n_digit_list) != self.n_digits).any():
                    # we have found a different number of digits. This can only be
                    # true of there are no leading zero because the zeros fill up to a
                    # fixed number of digits
                    self.log.info("Setting leading zero to false")
                    self.leading_zero = False

                self.file_number_list.append(int(file_number))

                if self.image_file_root is None:
                    # the first iteration the file root is None, so create the file
                    # root based on the first time step
                    self.image_file_root = rchop(self.file_base, file_number)
                    self.log.debug(
                        "sub {} {} {}".format(self.image_file_root, file_number, self.file_base))

                    # also filter the file name if a filter is given. Used to remove
                    # the _t leading the number of the file
                    if self.filter_base_name is not None:
                        filter_match = re.search(self.filter_base_name, self.image_file_root)
                        if bool(filter_match):
                            self.number_prefix = filter_match.group(1)
                            self.image_file_root_bare = re.sub(self.filter_base_name, "",
                                                               self.image_file_root)

            else:
                self.log.warning(
                    "No correct file number found for file: {}. Skipping".format(file1))
                continue

        self.file_number_list.sort()

        self.log.debug("file numbers: {}".format(self.file_number_list))
        self.step_size = np.diff(self.file_number_list)

    def make_link_list(self):
        """
        Create the symbolic links in case we are running on linux
        """

        # in case there is one step size in the image list larger than 1, all files
        # need to be copied to a new series with unit step size
        if (self.step_size > 1).any() and self.mode == "ffmpeg":
            # set the ffmpeg file root equal to the image base of the copied images
            self.image_file_root = self.link_base + self.number_prefix
            self.leading_zero = False

            self.log.debug("Creating  symbolic links for file conversion")
            for (cnt, file1) in enumerate(self.file_list):
                file_base, image_extension = os.path.splitext(file1)
                self.log.debug("extension {}".format(image_extension))

                file2 = "{}{}{:d}{}".format(self.link_base, self.number_prefix, cnt,
                                            image_extension)

                self.image_link_list.append(file2)

                cpcmd = "cp -v {} {}".format(file1, file2)
                self.log.info("{}".format(cpcmd))
                if not self.test:
                    if not os.path.exists(file2):
                        shutil.copy(file1, file2)
                    else:
                        self.log.info("File exists. Do not copy again")

            # in case we were reading from a file name, also dump the file names to a new file name
            # and update the filename
            base, ext = os.path.splitext(self.file_list_name)
            self.file_list_name = "_".join([base, self.link_base]) + ext
            with open(self.file_list_name, "w") as fp:
                for file_name in self.image_link_list:
                    fp.write(file_name)
        else:
            # all step size were 1, so we can use the original images
            self.log.debug("Directly convert images based on first file number")

    def make_output_filename(self):
        """
        Create the output file name based on the first image

        """

        if self.output is None:
            self.output = "{}.{}".format(self.image_file_root_bare, self.movie_type)
        else:
            self.movie_def, self.movie_ext = os.path.splitext(self.output)
            if self.movie_def != self.movie_type:
                self.log.warning("Movie type of output differs "
                                 "from option given by movie_type: {} vs {}".format(self.movie_type,
                                                                                    self.movie_def))

        if not re.search("\.\w+$", self.output):
            self.output += ".{}".format(self.movie_type)

        self.log.debug("file root: {}".format(self.image_file_root))
        self.log.debug("file root bare: {}".format(self.image_file_root_bare))

    def init_command(self):
        """
        Initialise the command list and start with echo if you want to test only switch mutual
        exclusive arguments
        """
        self.commands = []
        if self.test:
            self.commands.append("echo")

    def make_command(self):

        self.init_command()

        # create here the specific command depending on use of ffmpeg or mencoder
        self.log.debug("Creating command for {}".format(self.mode))
        if self.mode == "ffmpeg":
            self.make_ffmpeg_command()
        elif self.mode == "mencoder":
            self.make_mencoder_command()
        else:
            raise ValueError("mode should be either 'ffmpeg' or 'mencoder'")

    def make_mencoder_command(self):
        """
        Build the mencode command to be executated on the command line
        """
        self.log.debug("Creating mencoder command")

        # read the first image and get its dimensions
        self.log.debug("Analysing first image : {}".format(self.file_list[0]))
        im = Image.open(self.file_list[0])
        self.image_width = im.size[0]
        self.image_height = im.size[1]
        self.log.debug("Image size : {}".format(im.size))

        self.commands.append("mencoder")

        if self.file_list_name is None:
            # no file list name is given, so directly add to command line
            self.commands.append("mf://{}".format(",".join(self.file_list)))
        else:
            # a file list name is given. Store the file names as a list to this file and pass the
            # file list name
            # to the mencoder command line
            with open(self.file_list_name, "w") as fp:
                for file_name in self.file_list:
                    fp.write("{}\n".format(file_name))
            self.commands.append("mf://@{}".format(self.file_list_name))

        self.commands.extend(["-mf", "w={}:h={}:fps={}:type={}".format(self.image_width,
                                                                       self.image_height, self.fps,
                                                                       self.image_extension)])

        try:
            self.bitrate = (self.btr * self.fps * self.image_width * self.image_height) / 256
        except TypeError:
            self.bitrate = 100

        self.commands.extend(["-ovc", "lavc"])
        self.commands.extend(["-lavcopts",
                              "vcodec={}:vbitrate={}:mbd={}:autoaspect".format(self.codec,
                                                                               self.bitrate,
                                                                               self.mbd)])

        self.commands.extend(["-oac", "copy"])
        self.commands.extend(["-o", self.output])

    def make_ffmpeg_command(self):

        self.commands.append("ffmpeg")

        # build the ffmpeg filter
        zerod = ""
        if self.leading_zero:
            zerod = "0{}".format(self.n_digits)
        extension = self.image_extension
        if not re.match("^\.", self.image_extension):
            extension = "." + extension
        self.ffmpeg_filter = "{}%{}d{}".format(self.image_file_root, zerod, extension)

        # show some info for debugging
        self.log.debug("file root: {}".format(self.image_file_root))
        self.log.debug("n_digits: {}".format(self.n_digits))
        self.log.debug("leading zero: {}".format(self.leading_zero))
        self.log.debug("filter : {}".format(self.ffmpeg_filter))

        # set some more options
        if self.force:
            self.commands.append("-y")

        self.commands.extend(["-f", "image2"])

        if self.fps is not None:
            self.commands.extend(["-framerate", "{:d}".format(self.fps)])

        if self.high_quality:
            self.commands.extend(["-async", "1"])

        self.commands.extend(["-i", self.ffmpeg_filter])

        if os.name == 'nt':
            # on windows use the yuv420p codec
            self.commands.extend(["-pix_fmt", "yuv420p"])

        if self.frame_multiplication_factor is not None:
            self.commands.extend(["-filter:v \"setpts={:.2f}*PTS\""
                                  "".format(self.frame_multiplication_factor)])

        self.commands.append(self.output)

        if self.btr is not None:
            self.commands.extend(["-b:v", "{:d}".format(self.btr)])

        if self.high_quality:
            # commands.extend(["-b", "1000k", "-ab", "260k", "-ar", "84100"])
            # self.commands.extend(["-vb", "100M", "-qscale:v", "1"])
            # self.commands.extend(["-qscale:v", "3"])
            self.commands.extend(["-qscale:v", "3"])

    def run_command(self):

        self.log.info("{}".format(self.commands))

        # run the ffmpeg command
        try:
            subprocess.Popen(self.commands).wait()
        except SystemError:
            self.log.warning("failed making movie files")

    def clean_files(self):
        self.log.info("cleaning file list {}".format(self.image_link_list))
        for file_name in self.image_link_list:
            try:
                os.remove(file_name)
            except OSError:
                pass
        if self.file_list_name is not None:
            self.log.info("cleaning file list name {}".format(self.file_list_name))
            try:
                os.remove(self.file_list_name)
            except OSError:
                pass


def _parse_the_command_line_arguments(args):
    # retrieve the options from the command line
    parser = argparse.ArgumentParser(description=
                                     "A front end to create a ffmpeg movie using ffmpeg",
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("--version", help="Show the current version", action="version",
                        version="{}\nPart of hmc_tools version {}".format(
                            os.path.basename(__file__), __version__))
    # mutual exclusive parameters for append direction
    parser.add_argument("--output", type=str, help="Name of the output movie", default=None)

    # list of file names. Minimum of one should be given
    parser.add_argument("files", help="File pattern of all the file we want to process",
                        action="store")

    parser.add_argument("--test", help="Dry run, do not execute yet", action="store_true",
                        default=False)

    parser.add_argument("--high_quality", help="Add quality options to improve the movie",
                        action="store_true")

    parser.add_argument("--fps", type=int, default=10,
                        help="Defined the number of frames per second")

    parser.add_argument("--btr", type=int, help="Bit rate of the movie")

    parser.add_argument("--frame_multiplication_factor", default=None,
                        help="Multiplies the number of frames with this factor for proper speed up "
                             "(for <1) and slow down (for >1)", type=float)

    parser.add_argument("--movie_type", choices=["avi", "mp4", "mpg"], default=None,
                        help="Defined the movie types")

    parser.add_argument("--pattern", type=str, default="(\d+)$",
                        help="Give pattern for extracting the file number from the file name",
                        metavar="STRING")

    parser.add_argument("--filter_base_name", type=str, default="(\.|_)[ti]*$",
                        help="Give pattern for cleaning the remaining file base name after the "
                             "number has been removed. This filtered name is used for the default "
                             "filename", metavar="STRING")

    parser.add_argument("--link_base", type=str, default="image_link",
                        help="The base name of the symbolic links point to the original files",
                        metavar="STRING")

    parser.add_argument("--copy_files", type=bool, default=True,
                        help="Copy the image files in stead of links", metavar="STRING")

    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level", const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level", const=logging.WARNING)

    parser.add_argument("--keep_files", help="Keep the copied files",
                        action="store_false", dest="clean_files")

    parser.add_argument("--clean_files", help="Clean up the generated files", action="store_true",
                        default=True, dest="clean_files")

    parser.add_argument("--force", help="Overwrite existing file",
                        action="store_true", default=False)

    parser.add_argument("--mode", help="Type of movie maker to use", choices=["ffmpeg", "mencoder"],
                        default="ffmpeg")

    parser.add_argument("--file_list_name", action="store", type=str,
                        help="Create a movie via a file list")

    # parse the command line
    parsed_argumenents = parser.parse_args(args)

    return parsed_argumenents, parser


def rchop(string_in, ending):
    """
    Substitute only the last occurrence of the string 'ending' from the the string 'string_in'

    Parameters
    ----------

    string_in: str
        String from which to take out the substring
    ending:  str
        Substring to remove from string

    Returns
    -------
    str:
        String with the 'ending' string removed only from the first occurrence from the end
    """

    if string_in.endswith(ending):
        return string_in[:-len(ending)]
    return string_in


def main(args):
    args, parser = _parse_the_command_line_arguments(args)

    _logger = create_logger(console_log_level=args.log_level)

    _logger.debug("Creating movie with {}".format(args.file_list_name))

    movie = MovieInfo(args.files,
                      pattern=args.pattern,
                      filter_base_name=args.filter_base_name,
                      test=args.test,
                      mode=args.mode,
                      movie_type=args.movie_type,
                      link_base=args.link_base,
                      high_quality=args.high_quality,
                      fps=args.fps,
                      btr=args.btr,
                      force=args.force,
                      file_list_name=args.file_list_name)

    movie.make_command()
    movie.run_command()

    if args.clean_files:
        movie.clean_files()


def _run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == '__main__':
    _run()
