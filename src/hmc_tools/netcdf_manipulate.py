#!/bin/sh
"""
Read a series of netcdf data base files generated with *ftp_to_netcdf*, and do some manipulations
The NetCDF file contain lat/lon 2D planes vs a time coordinate. With this script you can sort the
time steps

"""

import argparse
import logging
import os
import sys
from glob import glob

import netCDF4 as nc
import pandas as pd
from hmc_utils.misc import (create_logger, get_logger,
                            print_banner, query_yes_no)

from hmc_tools import __version__


def ncdump(nc_fid, verb=True):
    """
    ncdump outputs dimensions, variables and their attribute information.
    The information is similar to that of NCAR's ncdump utility.
    ncdump requires a valid instance of Dataset.

    Parameters
    ----------
    nc_fid : netCDF4.Dataset
        A netCDF4 dateset object
    verb : Boolean
        whether or not nc_attrs, nc_dims, and nc_vars are printed

    Returns
    -------
    nc_attrs : list
        A Python list of the NetCDF file global attributes
    nc_dims : list
        A Python list of the NetCDF file dimensions
    nc_vars : list
        A Python list of the NetCDF file variables
    """

    def print_ncattr(key):
        """
        Prints the NetCDF file attributes for a given key

        Parameters
        ----------
        key : unicode
            a valid netCDF4.Dataset.variables key
        """
        try:
            print("\t\ttype:", repr(nc_fid.variables[key].dtype))
            for ncattr in nc_fid.variables[key].ncattrs():
                print('\t\t{} {}:'.format(ncattr, repr(nc_fid.variables[key].getncattr(ncattr))))
        except KeyError:
            print("\t\tWARNING: {} does not contain variable attributes".format(key))

    # NetCDF global attributes
    nc_attrs = nc_fid.ncattrs()
    if verb:
        print("NetCDF Global Attributes:")
        for nc_attr in nc_attrs:
            print('\t{}: {}'.format(nc_attr, repr(nc_fid.getncattr(nc_attr))))
    nc_dims = [dim for dim in nc_fid.dimensions]  # list of nc dimensions
    # Dimension shape information.
    if verb:
        print("NetCDF dimension information:")
        for dim in nc_dims:
            print("\tName: {}".format(dim))
            print("\t\tsize: {}".format(len(nc_fid.dimensions[dim])))
            print_ncattr(dim)
    # Variable information.
    nc_vars = [var for var in nc_fid.variables]  # list of nc variables
    if verb:
        print("NetCDF variable information:")
        for var in nc_vars:
            if var not in nc_dims:
                print('\tName: {}'.format(var))
                print("\t\tdimensions: {}".format(nc_fid.variables[var].dimensions))
                print("\t\tsize: {}".format(nc_fid.variables[var].size))
                print_ncattr(var)
    return nc_attrs, nc_dims, nc_vars


class NetCDFManipulate(object):
    """
    Manipulate NetCDF files

    Parameters
    ----------
    file_names: list
        List of data base file to import and manipulate
    output_file_name: str, optional
        Name of the resulting output file(s). Default "_new" is put behind the input name
    execute: bool, optional
        Immediately execute the processing steps. Default = False
    mode: {"info", "sort", "merge"}, optional
        Type of processing we have to carry out. The choices are

        - info: Show information on the data base
        - sort: Sort the time steps
        - merge: merge the data base files together based on their time index

        Default = "info"
    extension: str
        Extension to put to the file name if multiple files are process. Default = "new
    test: bool, optional
        Do not write anything to file, only show the processing steps. Default = False
    max_time_steps: int
        Maximum number of times to process
    start_date_time: str or int
        Start processing from this time or index (in case a int is given)
    end_date_time: str or int
        End processing until this time or index (in case a int is given)

    Examples
    --------

    This script is meant as a command line utility which can be called as::

        netcdf_manipulate data_base_1.nc data_base_2.nc --mode merge

    which will merge the databases *data_base_1.nc* and *data_base_2.nc* to a new database.

    """

    def __init__(self, file_names,
                 output_file_name=None,
                 execute=False,
                 mode="info",
                 test=False,
                 force=False,
                 extension="new",
                 max_time_steps=None,
                 start_date_time=None,
                 end_date_time=None
                 ):

        self.logger = get_logger(__name__)

        self.force = force
        self.max_time_steps = max_time_steps
        if start_date_time is not None:
            self.start_date_time = pd.Timestamp(start_date_time)
        else:
            self.start_date_time = None
        if end_date_time is not None:
            self.end_date_time = pd.Timestamp(end_date_time)
        else:
            self.end_date_time = None
        self.data_base_file_names = list()
        for file_name_pattern in file_names:
            files = glob(file_name_pattern)
            self.data_base_file_names.extend(files)

        self.test = test
        self.execute = execute
        self.mode = mode
        self.output_file_name = output_file_name
        self.extension = extension
        if output_file_name is not None:
            self.output_file_name = output_file_name
            extension = os.path.splitext(self.output_file_name)[1]
            if extension not in (".nc", ".NC", ".netcdf", ".NetCDF"):
                raise ValueError("Only NetCDF output type supported. Found '{}'".format(extension))
        else:
            self.output_file_name = self.make_output_file_name(self.data_base_file_names[0])

        # will contain all the databases per file name
        self.data_bases = dict()
        self.data_bases_out = dict()
        self.time_steps = dict()
        self.calender = None
        self.units = None

        # will contain the collection of all the database per file name
        self.data_base_total = dict()

        self.open_data_bases()

        if len(self.data_bases.keys()) == 0:
            raise ValueError("No data bases were loaded. Check your file names")

        if self.execute:
            if self.mode == "info":
                self.show_info()
            elif self.mode == "merge":
                self.logger.info("Merging the data")
            elif self.mode == "sort":
                self.init_data_bases()
                self.sort_time()
                self.write_data_bases()
            else:
                raise AssertionError("Not recognised mode : {}".format(self.mode))

        self.logger.info("Done.")

    def make_output_file_name(self, file_name):
        """
        Make a new file name based on the settings

        Parameters
        ----------
        file_name: str
            Input file name

        Returns
        -------
        str
            Output filename based on extension

        """
        out_file_base, ext = os.path.splitext(file_name)
        out_file = "_".join([out_file_base, self.extension]) + ext

        return out_file

    def open_data_bases(self):
        """
        Read all the data bases
        """
        for cnt, file_name in enumerate(self.data_base_file_names):
            self.logger.info("Opening NetCDF database {}: {}".format(cnt, file_name))
            data_set = nc.Dataset(file_name, "r")
            self.data_bases[file_name] = data_set
            if self.mode == "sort":
                out_file = self.make_output_file_name(file_name=file_name)
                if os.path.exists(out_file) and not self.force:
                    if query_yes_no("File {} already exist. Overwrite ?".format(out_file)) == "no":
                        sys.exit("Stop processing here.")

                self.logger.info("Open new data base {}".format(out_file))
                if not self.test:
                    self.data_bases_out[file_name] = nc.Dataset(out_file, "w",
                                                                format=data_set.data_model)
                else:
                    self.data_bases_out[file_name] = None
                    self.logger.info("In test mode: do not open {}".format(out_file))

    def init_data_bases(self):

        for cnt, (file_name, data_set) in enumerate(self.data_bases.items()):
            data_set_out = self.data_bases_out[file_name]
            if data_set_out is None:
                continue

            # try to copy the global data set attributes
            try:
                data_set_out.description = data_set.description
                self.logger.info("Added description {}".format(data_set.discription))
            except AttributeError:
                self.logger.info("No description found")
            try:
                data_set_out.history = data_set.history + "\nModified by netcdf_manipulate at " \
                                                          "{}".format(pd.to_datetime("now"))
                self.logger.info("Added history {}".format(data_set_out.history))
            except AttributeError:
                self.logger.info("No history")

            try:
                data_set_out.source = data_set.source
                self.logger.info("Added source {}".format(data_set_out.source))
            except AttributeError:
                self.logger.info("No source")

            # always start with the time dimension. Make the size None so it is unlimited
            # create the dimension with the variable belonging to it
            data_set_out.createDimension("time", None)
            time = data_set_out.createVariable("time", data_set["time"].dtype, ("time",), zlib=True)
            # copy all the attributes of the time dimension (units and calender)
            for attribute in data_set.variables["time"].ncattrs():
                self.logger.info("Setting time attribute {}".format(attribute))
                time.setncattr(attribute, data_set.variables["time"].getncattr(attribute))

            self.logger.info("Init data base {}: {} ".format(cnt, file_name))
            for dim_name, data_in in data_set.dimensions.items():
                if dim_name == "time":
                    # we already initialised the time dimension. go to the next
                    continue
                self.logger.info("Adding dimension {}".format(dim_name))
                data_set_out.createDimension(dim_name, data_in.size)
                data_in_var = data_set.variables[dim_name]
                data_out = data_set_out.createVariable(dim_name, data_in_var.dtype,
                                                       (dim_name,), zlib=True)
                for attribute in data_in_var.ncattrs():
                    attr_val = data_in_var.getncattr(attribute)
                    self.logger.info("Setting dim attribute {} : {} {}".format(dim_name,
                                                                               attribute,
                                                                               attr_val))
                    data_out.setncattr(attribute, attr_val)

                # copy the dimension data from in to out
                data_out[:] = data_in_var[:]

            dim_strings = [dim_name for dim_name in data_set.dimensions.keys()]
            for var_name, data_in in data_set.variables.items():
                if var_name not in data_set.dimensions.keys():
                    self.logger.info("Adding variable {}".format(var_name))
                    # get a reference to the current variable in the inptu
                    data_in_var = data_set.variables[var_name]
                    # create a new variable with the dimensions given by the dim_strings which are
                    # taken from the input: time, lat, lon
                    data_out = data_set_out.createVariable(var_name, data_in.dtype, dim_strings,
                                                           zlib=True)
                    for attribute in data_in_var.ncattrs():
                        attr_val = data_in_var.getncattr(attribute)
                        self.logger.info("Setting var attribute {} : {} {}".format(var_name,
                                                                                   attribute,
                                                                                   attr_val))
                        data_out.setncattr(attribute, attr_val)

    def sort_time(self):
        """
        Loop over all the imported data bases and correct the time zone
        """
        self.logger.info("Sort the time steps")
        for cnt, (file_name, data_set) in enumerate(self.data_bases.items()):
            self.logger.info("Sorting {}: {} ".format(cnt, file_name))

            nc_time = data_set.variables["time"]

            time = nc_time[:]
            self.calender = nc_time.getncattr("calendar")
            self.units = nc_time.getncattr("units")

            date_time = nc.num2date(time, units=self.units, calendar=self.calender)
            time_df = pd.DataFrame(index=date_time, data=time, columns=["nc_time"])
            time_df["i_time"] = [i for i in range(time_df.index.size)]
            time_df.index.name = "DateTime"
            time_df.sort_index(inplace=True)
            time_df["file_name"] = [file_name for i in range(time_df.index.size)]
            self.time_steps[file_name] = time_df

    def write_data_bases(self):
        """
        Write all the data base to file
        """

        for cnt, (file_name, data_set) in enumerate(self.data_bases.items()):
            self.logger.info("Writing data base {}: {} ".format(cnt, file_name))

            data_set_out = self.data_bases_out[file_name]
            if not self.test:
                data_out_time = data_set_out.variables["time"]
            else:
                data_out_time = None

            i_time_out = 0
            for (index, row) in self.time_steps[file_name].iterrows():

                if self.start_date_time is not None:
                    if pd.Timestamp(index) < self.start_date_time:
                        continue
                if self.end_date_time is not None:
                    if pd.Timestamp(index) >= self.end_date_time:
                        break

                if self.max_time_steps is not None and i_time_out >= self.max_time_steps:
                    self.logger.info("Maximum number of time steps to process reach at {} {}. Quit"
                                     "".format(i_time_out, index))
                    break

                nc_time = row["nc_time"]
                i_time_in = row["i_time"]
                self.logger.info("writing row {} : {} ".format(index, nc_time))

                for var_name, data_in in data_set.variables.items():
                    if var_name not in data_set.dimensions.keys():
                        self.logger.debug("Adding variable {}".format(var_name))
                        try:
                            # get a reference to the current variable in the inptu
                            data_in_var = data_set.variables[var_name][i_time_in, ...]
                        except RuntimeError:
                            self.logger.warning("Failed to load variable {} at {} ({}). Skipping"
                                                "".format(var_name, index, i_time_in))
                            break
                        else:
                            if not self.test:
                                data_out_time[i_time_out] = nc_time
                                data_out_var = data_set_out.variables[var_name]
                                data_out_var[i_time_out, ...] = data_in_var

                i_time_out += 1

        self.logger.info("Done")

    def show_info(self):
        """
        Show information per file name
        """

        for cnt, (file_name, data_set) in enumerate(self.data_bases.items()):
            print_banner(" db {}: {}".format(cnt, file_name))

            ncdump(data_set, verb=True)

            nc_time = data_set.variables["time"]

            time = nc_time[:]
            try:
                calender = nc_time.getncattr("calendar")
            except AttributeError:
                self.logger.info("No calendar can be found in the netcdf file. Do you have "
                                 "relative data?")
                calender = None

            try:
                units = nc_time.getncattr("units")
            except AttributeError:
                self.logger.info("No units can be found in the netcdf file. ")
                units = None

            try:
                start_date_num = data_set.start_date_time
            except AttributeError:
                self.logger.info("No start date/time can be found in the netcdf file. ")
            else:
                # convert start date/time numerical value. Assume calendar and units
                self.start_date_time = pd.Timestamp(nc.num2date(start_date_num,
                                                        units="hours since 0001-01-01 00:00:00",
                                                        calendar="gregorian"), tz="UTC")
                self.logger.info("Found start date/time the netcdf file. ")

            if calender is not None and units is not None:
                date_time = nc.num2date(time, units=units, calendar=calender)
            else:
                if self.start_date_time is not None:
                    date_time = [self.start_date_time + pd.Timedelta(t, unit="h") for t in time]
                else:
                    self.logger.info("No start date/time found as well. Just copy date time")
                    date_time = time

            self.logger.info("Time steps:")
            for ii, ts in enumerate(time):
                if self.start_date_time is not None:
                    append = " from start at {}".format(self.start_date_time)
                else:
                    append = " in {} (calender: {})".format(units, calender)
                self.logger.info("time {} : {} h {}".format(date_time[ii], ts, append))


def _parse_command_line_arguments(args):
    """ Parser for utility """
    parser = argparse.ArgumentParser(description="A script for manipulate the fatigue database",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--version", help="Show the current version", action="version",
                        version="{}\nPart of hmc_tools version {}".format(
                            os.path.basename(__file__), __version__))
    # set the verbosity level command line arguments
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level",
                        const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level",
                        const=logging.WARNING)

    parser.add_argument('file_names', nargs="+",
                        help="Name of the excel data base to manipulate settings. Can be given "
                             "with * to add multiple files or added several times on the command "
                             "line")
    parser.add_argument('--mode', help="Type of operation to carry out",
                        choices=["info", "merge", "sort"], default="info")
    parser.add_argument('--test', help="Only do what you want to do, do not write to file",
                        default=False, action="store_true")
    parser.add_argument('--force', help="Force to overwrite a new file, even if it already exist",
                        default=False, action="store_true")
    parser.add_argument('--out_file', help="Name of the text output file")
    parser.add_argument('--max_time_steps', type=int, default=None,
                        help="Limit the number of time steps to process to this maximum number of "
                             "time steps")
    parser.add_argument('--extension', default="new",
                        help="String to put behind the  output file name when the output file "
                             "name is not explicitly defined")
    parser.add_argument('--start_date_time',
                        help="If given, process the times with a time stamp equal "
                             "or large than the *monitoring_start_date_time*")
    parser.add_argument('--end_date_time',
                        help="If given, only process the times with a time stamp smaller "
                             "than the *monitoring_end_date_time*. Equal times are not included")

    # parse the command line
    parsed_argumenents = parser.parse_args(args)

    return parsed_argumenents, parser


def main(args):
    args, parser = _parse_command_line_arguments(args)

    create_logger(console_log_level=args.log_level,
                  file_log_format_long=False, console_log_format_clean=True)

    NetCDFManipulate(
        file_names=args.file_names,
        output_file_name=args.out_file,
        execute=True,
        mode=args.mode,
        test=args.test,
        force=args.force,
        extension=args.extension,
        max_time_steps=args.max_time_steps,
        start_date_time=args.start_date_time,
        end_date_time=args.end_date_time
    )


def _run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == '__main__':
    _run()
