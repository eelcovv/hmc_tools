#!/bin/sh
"""
Read a netcdf file containing the 3D forecast and interpolate the sea states along the route given
in a separate excel or kml file

Notes
--------

This module provides a new class called :class:`RouteInterpolator` and a function in order to
externally call the class and interpolate the data from the command line.
We can use the script in 3 ways:

1. Interpolate the sea states stored in a netcdf nowcast file along a route which have already
   traveled. In this case, provide the route in an excel data base containing as sheet called
   'General' with three columns: 'DateTime', 'GPS_LATITUDE', and 'GPS_LONGITUDE'. The names can be
   altered by passing the '--longitude_name', '--latitude_name' and the
   '--date_time_column_name' command line arguments. An example would be::

        >>> route_interpolate.exe  nowcast_database.nc --trajectory database_with_latlon_vs_time.xls

   In this example the *nowcast_database.nc* contains the 2D sea states for the nowcast times and
   *database_with_latlon_vs_time.xls* contains the trajectory with the latitude and longitude vs
   date/time

2. Interpolate the forecast sea states along a route which we still need to travel. In this case,
   only the latitude and longitude of the route needs to be given, but not the date/time. In stead
   a sailing speed must be given which is internally used to obtain the coordinates vs time given
   the sailing speed. In this mode, we can pass the coordinates via a Google Earth KML file, for
   example::

        >>> route_interpolate.exe  forecast_data.nc --trajectory google_earth_route.kml

    The *forecast_data.nc* is the net cdf data base with the forecast vs time relative to the
    start_date_time as defined in the *start_date_time* attribute of the netcdf file. The trajectory
    can be created in Google Earth by placing the yellow markers and export a list of markers to
    kml file. The default sailing speed of '6 knots' is used to internally calculate the locations
    of the vessel vs date/time. This values can be changed using the '--speed'  argument.


3. Interpolate the sea states at a fixed location. In this mode, no '--trajectory_route_file' needs
   to be given at all, we just define the coordinates via the  '--latitude' and '--longitude'
   command line examples::

        >>> route interpolate.exe nowcast database.nc --longitude "62 59 0 W" --latitude "11 59 9 N"

   In this case we have based the coordinates given on by the latitude and longitude arguments.
   Note that we can define the format of the geo coordinates with the *--format_lat_lon* argument.
   In this case we have to pass the degrees, minutes, seconds and hemnisphere as a string separated
   by spaces. In case floats are passed by default a decimal notation of the coordinates is assumed

"""
from __future__ import division
from __future__ import print_function

import argparse
import logging
import os
import re
import sys

import geopy
import netCDF4 as nc
import numpy as np
import pandas as pd
import progressbar as pb
import xarray as xr
from LatLon.LatLon import (LatLon, Latitude, Longitude, string2latlon)
from geopy.distance import VincentyDistance
from hmc_utils import Q_
from hmc_utils.geographic import (travel_distance_and_heading_from_coordinates, import_way_points,
                                  get_speed_from_distance_and_time)
from hmc_utils.misc import (create_logger, get_logger, print_banner, set_default_dimension)
from pytz import timezone

from hmc_tools import __version__

__author__ = "Eelco van Vliet"
__copyright__ = "Eelco van Vliet"
__license__ = "mit"

_logger = logging.getLogger(__name__)

# set up progress bar properties
PB_WIDGETS = [pb.Percentage(), ' ', pb.Bar(marker='.', left='[', right=']'), ""]


def progress_bar_message(cnt, total):
    return "Processed time {:d} of {:d}".format(cnt + 1, total)


class RouteInterpolator(object):
    """
    Class to do the interpolation of routes

    Parameters
    ----------
    netcdf_input_file: str
        Name of the netcdf file containing the sea states to interpolate. The times can be absolute
        and relative. This argument is the only mandatory argument. The netcdf file can both have an
        absolute and relative time along the time axis. In case a relative time is given, the
        starting time must be given in the `start_date_time` attribute field of the net cdf file.
        The existence of this attribute is also a indicator that we are dealing with relative
        times. The netcdf file can be generated with the `ftp_to_netcdf` tool in this package.
    trajectory_input_file: str, optional
        Name of the file containing the coordinates along the route where we want to interpolate
        Could ba a excel file with coordinates vs date/time or even a kml file without date/times
    replace_gps_zero_with_nan: bool, optional
        If true, all the zero gps coordinates (0,0) are converted to nan. Default = True
    clip_speed: bool, optional
        Clip all velocity with a value higher than this velocities. Default = 100 m/s
    speed_name: str, optional
        Name of the speed column in knots. Default = "speed_sustained"
    file_id: str, optional
        An extra string we want to append to the end of the output file name
    file_appendix: str, optional
        In case the output name is the same as the input, append this string unless overwrite is
        True. Default = "seastates"
    overwrite: bool, optional
        Overwrite the input trajectory file. Default = False
    lat_lon_division: float, optional
        Scale the latitude/longitude by this factor (can be 100.0 sometimes). Default = 1
    progressbar: bool, optional
        Show a progress bar during processing. Default = False
    heading_name: str, optional
        Name of the heading column in the input file. Default = "heading"
    latitude_name: str, optional
        Name of the latitude column in the input file. Default = "latitude"
    longitude_name: str, optional
        Name of the longitude column in the input file. Default = "longitude"
    start_way_point: int, optional
        Start at this way point. Default = 0
    number_of_sample_points: int, optional
        Number of samples put along the kml way point
    speed: :class:`Units`, optional
        Speed to use on the trajectory if no coordinates are present yet and we want to estimate
        the future location along the time axis given a route and certain speed. Note that the
        speed can have units attached, so it must be given as a string, i.e. "10 knots"
    average_window_length: float, optional
        To determine the speed at the end of a interpolated route we can define the average window
        length in hours. Default = 10
    start_distance: float, optional
        Start at this distance
    date_time_start: str, optional
        Start interpolation at this date/time given as a string, e.g. 20150425T0315000. If not
        given interpolation starts at the first time step of the netcdf file
    date_time_end: string, optional
        End interpolation at this date/time given as a string, e.g. 20150425T0315000. If not
        given interpolation ends at the last time step of the netcdf file
    max_relative_time: int, optional
        Maximum number of hours to interpolate. Default = none, so do not limit
    output_file_name: str, optional
        Name of the file to export to. If not given, it is constructed from the trajectory name
    base_name_output_file: str, optional
        Base name of the file in case we do not have a trajectory name as input
    date_time_column_name: str, optional
        Name of the date time column in the excel data base. Default = "DateTime"
    date_time_substitute: str, optional
        Apply  this substitute string to clean the date time. Default = "\.", i.e. remove dots in
        the date time strings, such that 2017.10.11 becomes 20171011
    general_sheet_name: str, optional
        The name of the sheet containing the lat/lon information to sample. Default = "General"
    information_sheet_name: str, optional
        The name of the sheet containing the running information. Default = "Information"
    heading: float, optional
        You can pass the last heading
    coordinates:  :class:`LatLon`, optional
        If no trajectory has been given the sea state are interpolated at fixed coordinates. In case
        we do have a trajectory file, only the trajectory starting at this coordinates is sampled.
    min_search_distance_for_cutting: float
        Distance in nm of the area around the coordinates (latitude, longitude) we are seaching
        to cut the trajectory
    """

    def __init__(self,
                 netcdf_input_file=None,
                 trajectory_input_file=None,
                 coordinates=None,
                 heading=None,
                 replace_gps_zero_with_nan=True,
                 clip_speed=Q_("100 m/s"),
                 file_id=None,
                 file_appendix="sea_states",
                 overwrite=False,
                 lat_lon_division=None,
                 progressbar=False,
                 heading_name="heading",
                 latitude_name="Latitude",
                 longitude_name="Longitude",
                 start_way_point=None,
                 number_of_sample_points=2000,
                 speed=Q_("6 knots"),
                 average_window_length=10,
                 start_distance=Q_("0 nautical_mile"),
                 date_time_start=None,
                 date_time_end=None,
                 max_relative_time=None,
                 output_file_name=None,
                 base_name_output_file="route",
                 date_time_column_name="DateTime",
                 speed_name="speed_sustained",
                 travel_distance_name="travel_distance",
                 distance_from_position_name="distance_from_position",
                 delta_distance_wrt_position_name="delta_distance_wrt_position",
                 min_search_distance_for_cutting=300,
                 travel_time_name="travel_time",
                 date_time_substitute="\.",
                 general_sheet_name="General",
                 information_sheet_name="Information",
                 write_to_file=False,
                 ):
        self._logger = get_logger(__name__)

        # file name of the netcdf to read
        self.netcdf_input_file = netcdf_input_file

        # file name of the file with the coordinates along which we want to interpolate. Can be
        # an excel file or an kml file
        if trajectory_input_file is not None:
            self.trajectory_input_file = trajectory_input_file
            _file_base, _file_extension = os.path.splitext(trajectory_input_file)
            self.trajectory_input_file_base = _file_base
            self.trajectory_input_file_extension = _file_extension
            self.trajectory_type = None

            if bool(re.search("xls", _file_extension, re.IGNORECASE)):
                self.trajectory_type = "xls"
            elif bool(re.search("kml", _file_extension, re.IGNORECASE)):
                self.trajectory_type = "kml"
            else:
                raise NotImplementedError("Trajectory input file only implemented for xls or kml."
                                          "Found {} ".format(_file_extension))
        else:
            self.trajectory_input_file = None

        # in case fixed coordinates are given, we interpolate at just one single point
        self.coordinates = coordinates
        self.heading = heading

        # all the other options
        self.replace_gps_zero_with_nan = replace_gps_zero_with_nan
        self.clip_speed = clip_speed
        self.speed_name = speed_name
        self.travel_distance_name = travel_distance_name
        self.distance_from_position_name = distance_from_position_name
        self.delta_distance_wrt_position_name = delta_distance_wrt_position_name
        self.travel_time_name = travel_time_name
        self.file_id = file_id
        self.file_appendix = file_appendix
        self.overwrite = overwrite
        self.lat_lon_division = lat_lon_division
        self.progressbar = progressbar
        self.heading_name = heading_name
        self.latitude_name = latitude_name
        self.longitude_name = longitude_name
        self.start_way_point = start_way_point
        self.number_of_sample_points = number_of_sample_points
        self.speed = speed
        self.start_distance = start_distance
        self.date_time_start = date_time_start
        self.date_time_end = date_time_end
        self.max_relative_time = max_relative_time
        self.output_file_name = output_file_name
        self.base_name_output_file = base_name_output_file
        self.date_time_column_name = date_time_column_name
        self.date_time_substitude = date_time_substitute
        self.general_sheet_name = general_sheet_name
        self.information_sheet_name = information_sheet_name
        self.write_to_file = write_to_file

        self.average_window_length = average_window_length

        self.min_search_distance_for_cutting = min_search_distance_for_cutting

        self.tz_local = timezone("UTC")

        # keep a counter of the number of valid sample we have found.
        self.n_valid_samples = 0

        self.trajectory_data_frame = None
        self.trajectory_date_time_index = None

        # will contain the other sheet if we read from the excel file
        self.data_sheets = None

        # will contain the trajectory data at the sea state times
        self.trajectory_at_sea_state_time = None

        # contains the sea state date over lat/lon vs time
        self.sea_states_3d = None

        # the absolute date/time of the sea states time step
        self.sea_states_date_times = None
        self.sea_states_delta_time = None

        # this is the start date time
        self.start_date_time = None

        # store the first and last date time. Either the date_time_start given by the user or the
        # first or last in  the netcdf file
        self.first_date_time = None
        self.last_date_time = None

        # in case the netcfd has relative time wrt a start time, set this flag to true
        self.have_relative_times = None

        # we always have to read a netcdf file with sea states
        self.import_netcdf_file()

        # import the trajectory if given on the command line
        if self.trajectory_input_file:
            self.import_trajectory()
        else:
            # we have only added coordinates at the command line. Just interpolate at this location
            self.create_fixed_location_data_frame()

        if self.output_file_name is None:
            # we have not defined an output file name. Base it on the input name
            self.create_output_file_name()

        # call the method to interpolate the sea states from the netcdf 3d data to the trajectory
        self.interpolate_sea_states_to_trajectory()

        # create some message to screen
        self.report_last_position_and_speed()

        if self.write_to_file:
            # In case the write_to_file flag is given, immediately write to file
            self.export_trajectory()

    def create_output_file_name(self):
        """
        Create the output file name
        """

        if self.trajectory_input_file is not None:
            file_base = os.path.splitext(self.trajectory_input_file)[0]
        else:
            file_base = self.base_name_output_file

        if self.file_id is not None:
            file_base += "_" + self.file_id

        if self.date_time_start is not None:
            file_base += "_from_" + self.first_date_time.tz_convert("UTC").strftime("%Y%m%d")

        if self.date_time_end is not None:
            file_base += "_until_" + self.last_date_time.tz_convert("UTC").strftime("%Y%m%d")

        # in case the output file would be the same as the input, add a appendix
        file_base += "_" + self.file_appendix

        if not self.overwrite:
            self.output_file_name = file_base + ".xls"
        else:
            self.output_file_name = self.trajectory_input_file

    def import_netcdf_file(self):
        """
        Import the sea states from the NETCDF file.

        Notes
        -----
        * The time axis can have relative and absolute time.
        * The absolute times are stored to the *sea_states_date_times* attribute
        * In case we are dealing with relative times, set the *have_relative_times* flag to True
        """

        _logger.info("Reading seastates from {}...".format(self.netcdf_input_file))
        self.sea_states_3d = xr.open_dataset(self.netcdf_input_file, decode_times=True)

        # the sea state forecast are relative to the time stored in the start_data_time
        # attribute
        _logger.debug(
            "seastate forecast times: {}".format(type(self.sea_states_3d["time"].values[0])))
        try:
            _start_date_time_num = self.sea_states_3d.attrs["start_date_time"]
        except KeyError:
            # we could not get the start_date_time key, which means that the times in the netcdf
            # are absolute already
            _date_times = np.array(
                [pd.Timestamp(tme, tz="UTC") for tme in self.sea_states_3d.coords["time"].values])
            self.have_relative_times = False
        else:
            # we have the start date time stored in the netcdf file, which means the times are
            # relative to this time. Now calculate the absolute date times
            # based on the start time and current time index. First convert the numerical value to
            # a pandas Datetime stamp
            self.start_date_time = pd.Timestamp(nc.num2date(_start_date_time_num,
                                                            units="hours since 0001-01-01 00:00:00",
                                                            calendar="gregorian"), tz="UTC")
            _sea_states_relative_time = np.array(
                [pd.Timedelta(tme, unit="h") for tme in self.sea_states_3d.coords["time"].values])
            _date_times = _sea_states_relative_time + self.start_date_time
            self.have_relative_times = True
        _logger.info("Succeeded")
        _logger.debug("Absolute times:\n{}".format(self.sea_states_date_times))
        _logger.debug("Have relative times: {}".format(self.have_relative_times))

        if self.date_time_start is not None:
            # no start time is given by the user. Take the first in the list
            self.first_date_time = pd.Timestamp(self.date_time_start, tz=self.tz_local)
        else:
            # just get the first sea state as a start time
            self.first_date_time = _date_times[0]

        self.first_date_time = self.first_date_time.tz_convert("UTC")

        if self.max_relative_time is not None:
            self.date_time_end = self.first_date_time + pd.Timedelta(self.max_relative_time, "h")

        if self.date_time_end is not None:
            # no end time is given by the user. Take the last in the list
            self.last_date_time = pd.Timestamp(self.date_time_end, tz=self.tz_local)
        else:
            self.last_date_time = _date_times[-1].tz_convert("UTC")

        # make a selection of data base times based on the first_date_time value we have found
        time_mask = (_date_times >= self.first_date_time) & (_date_times <= self.last_date_time)
        self.sea_states_date_times = _date_times[time_mask]
        self.sea_states_delta_time = self.sea_states_date_times - self.sea_states_date_times[0]

    def create_fixed_location_data_frame(self):
        """
        For a fixed location create a data frame with the times based on the sea states date/times
        an with a fixed location
        """

        # create a new data frame which is going to hold the sampled data of the sea state  times
        # based on netcdf database. It as the lat/on columns but these are empty
        self.trajectory_data_frame = pd.DataFrame(index=self.sea_states_date_times)
        self.trajectory_data_frame.index.name = self.date_time_column_name

        # store the lat/lon locations as a fixed value ni the data frame
        self.trajectory_data_frame[self.latitude_name] = self.coordinates.lat.decimal_degree
        self.trajectory_data_frame[self.longitude_name] = self.coordinates.lon.decimal_degree
        self.trajectory_date_time_index = self.trajectory_data_frame.index

    def cut_trajectory_at_coordinates(self):
        """
        In case we have read a trajectory and also defined the coordinates we also want to
        cut the trajectory at the coordinates
        """
        _logger.info("Cutting trajectory at location {}".format(self.coordinates.to_string('D')))

        self.trajectory_data_frame[self.distance_from_position_name] = np.nan

        # for all the locations, store the distance of the coordinates to the location where
        # we want to cut
        for cnt, (index, row) in enumerate(self.trajectory_data_frame.iterrows()):
            # get the latitude/longitude at the current date/time (ie row in the dataframe)
            latitude = row[self.latitude_name]
            longitude = row[self.longitude_name]
            position = LatLon(latitude, longitude)

            # get the distance between the location 'coordinates' and 'position'
            distance = self.coordinates.distance(position)
            self.trajectory_data_frame.ix[index, self.distance_from_position_name] = distance

        # now, get the differential difference. We use this to see if we are approaching the vessel
        # (then diff < 0) or going away from the vessel (then diff>0)
        distance_from_vessel = self.trajectory_data_frame[self.distance_from_position_name]
        delta_distance = distance_from_vessel.diff()

        # only check the delta distance at most two time the min distance from the vessel
        max_range_check = max(
            2 * self.trajectory_data_frame[self.distance_from_position_name].min(),
            self.min_search_distance_for_cutting
        )

        # set the part of the trajectory where we have passed the location of the vessel to true.
        # All position further away than 100 nm al always false to avoid problems at the start of
        # the voyage were we may have an increasing distance when the vessel leave the harbour.
        # Only at the straight line where we are around the vessel the differential of the distance
        # changes sign. The first index where the sign changes is the location of the vessel
        trajectory_after_vessel = (distance_from_vessel < max_range_check) & (delta_distance > 0)
        index_of_trajectory = self.trajectory_data_frame.index[trajectory_after_vessel].tolist()
        try:
            # get the first location where the signal of the differential of the distance changes
            index_of_vessel = index_of_trajectory[0]
        except IndexError:
            # we could not find the vessel. Do nothing and keep the whole journey
            pass
        else:
            # clip the trajectory at the location of the coordinates, ie where the delta is positive
            self.trajectory_data_frame = self.trajectory_data_frame.ix[index_of_vessel:]

            # update the distance index
            self.trajectory_data_frame.index -= self.trajectory_data_frame.index[0]

    def import_trajectory(self):
        """
        Import the trajectory from either kml or excel

        * In case no date/times can we found, the *trajectory_date_times* attributes will be None

        """

        _logger.info("Reading monitoring file {}".format(self.trajectory_input_file))

        if self.trajectory_type == "xls":
            # we have an excel file. Load the file and sea if we have date times. If so,
            # store it to the trajectory_date_times attribute
            # contains all the sheets of the excel data base
            self.data_sheets = pd.read_excel(self.trajectory_input_file, sheet_name=None)

            # select the sheet we need, which is the sheet with the name of the contents
            # of the general_sheet_name input argument, which is 'General' by default. This sheet
            # should contain the Lat/Lon information we need to get the trajectory
            self.trajectory_data_frame = self.data_sheets[self.general_sheet_name]

            date_time_index = self.get_date_time_index(self.trajectory_data_frame,
                                                       self.date_time_column_name,
                                                       self.date_time_substitude)
        else:
            # we are reading from an kml file. Create a data frame and read the points
            self.trajectory_data_frame = import_way_points(
                file_name=self.trajectory_input_file,
                start_wp=self.start_way_point,
                n_distance_points=self.number_of_sample_points,
                latitude_name=self.latitude_name,
                longitude_name=self.longitude_name,
                heading_name=self.heading_name,
                travel_distance_name=self.travel_distance_name
            )
            self.data_sheets = None
            date_time_index = None
            if self.coordinates is not None:
                # we have passed both a trajectory and coordinates at the command line, which means
                # we interpolate starting from this coordinates. Cut the first part of the journey
                try:
                    self.cut_trajectory_at_coordinates()
                except IndexError:
                    _logger.warning("Nothing to cut. Empty trajectory")

        if date_time_index is not None:
            # store the original data  time index of the data set
            self.trajectory_date_time_index = date_time_index
            # we have found date times in our trajectory input. Make sure to set it as the index
            self.trajectory_data_frame[self.date_time_column_name] = date_time_index
            self.trajectory_data_frame.set_index(self.date_time_column_name,
                                                 drop=True, inplace=True)
            # also impose the same date/time index on all the other data sheets
            for db_name, db in self.data_sheets.items():
                if self.date_time_column_name in db.columns:
                    db[self.date_time_column_name] = date_time_index
        else:
            # we did not find date/times in the trajectory file. Take the start time from the
            # netcdf data base
            self.lat_lon_at_times_of_sea_states()
            self.trajectory_date_time_index = self.trajectory_data_frame.index

    def get_date_time_index(self, data_frame, date_time_name, date_time_sub_regex="\."):
        """
        Get the date times from the data frame and covert to UTC

        Parameters
        ----------
        data_frame: :class:`Dataframe`
            Data frame containing the date time index
        date_time_name: str
            Name of the date time column
        date_time_sub_regex: str
            Regular expression use to clean the date time string. Default = "\." which removes
            all the dots from the date time strings

        Returns
        -------
        None or DatetimeIndex
            In case of success, return a date time index. Otherwise None

        Notes
        -----
        * This function also takes care of cleaning the Date/Time string in case not a standard
          format is used. For that, the 'date_time_sub_regex' is used which gives the string pattern
          which must be removed from the date/time string. This is normally a "\." to remove all
          the dots from a date/time string which some people like to put in but unfortunately is
          not standard
        """

        try:
            _dt = data_frame[date_time_name]
        except KeyError:
            date_time_index = None
        else:
            # here the clean of the date time takes place as all
            _date_times = [pd.Timestamp(re.sub(date_time_sub_regex, "", tme)) for tme in _dt]

            date_time_index = pd.DatetimeIndex(_date_times, name=date_time_name)

            if date_time_index.tz is None:
                # only convert to the given time zone if no time zone have been given yet
                date_time_index = date_time_index.tz_localize(self.tz_local, ambiguous='NaT')

            # now convert it to utc
            date_time_index = date_time_index.tz_convert("UTC")

        return date_time_index

    def interpolate_data_base_of_times(self):
        """
        Interpolate the data base with sea states at the data base times (which are the date/times
        of the 2D data planes stored in the netcdf file)

        In this method we assume we have a data/frame with coordinates along the journey with
        data/times corresponding with date/times in the sea state data base. We can therefore
        interpolate the sea states at the location
        """

        logger = get_logger(__name__)

        n_rows = len(self.trajectory_data_frame.index)

        # set up progress bar for this series of forecast times
        if self.progressbar:
            progress = pb.ProgressBar(widgets=PB_WIDGETS, maxval=n_rows, fd=sys.stdout).start()
            PB_WIDGETS[-1] = progress_bar_message(0, n_rows)
            progress.update(0)
            sys.stdout.flush()

        # loop over the rows of this trajectory data frame and process the latitude/longitude
        logger.info("Interpolating sea states along {} point of route...".format(n_rows))

        self.n_valid_samples = 0

        # loop over the date/times of the sea state data file
        for cnt, (index, row) in enumerate(self.trajectory_at_sea_state_time.iterrows()):
            sampled_date_time = index

            if self.have_relative_times:
                # the sea state 3d date are stored with a relative time. Calculate the relative
                # time
                logger.debug("sub {} and {}".format(sampled_date_time, self.start_date_time))
                sampled_date_time -= self.start_date_time
                if sampled_date_time < pd.Timedelta(0, "h"):
                    # a negative relative time is not possible. Continue
                    continue
            # get the latitude/longitude at the current date/time (ie row in the dataframe)
            latitude = row[self.latitude_name]
            longitude = row[self.longitude_name]

            # try to get the data at this time
            try:
                logger.info("Retrieving slice {}".format(sampled_date_time))
                # logger.debug("with sea_states_times {}".format(sea_states["time"]))
                data_sets_2d = self.sea_states_3d.sel(time=sampled_date_time).drop("time")
            except KeyError:
                logger.debug("No slice found for {}".format(sampled_date_time))
                # if it fails we can continue to the next
                continue

            logger.debug(
                "INTERP {} at time {} lat/lon {} {} ".format(cnt, sampled_date_time, latitude,
                                                             longitude))
            for quant_name, data_array in data_sets_2d.data_vars.items():
                try:
                    value_at_position = data_array.sel(latitude=latitude, longitude=longitude,
                                                       method="nearest")
                except AssertionError:
                    # backward compatibility with xarray 0.8 -> 0.9
                    value_at_position = data_array.sel(latitude=[latitude], longitude=[longitude],
                                                       method="nearest")

                if np.isnan(value_at_position):
                    logger.debug("No value of {} at {}, {}".format(quant_name, latitude, longitude))
                    continue
                logger.debug(
                    "Value of {} lat={} lon={} : {}".format(quant_name, latitude, longitude,
                                                            value_at_position.values))

                self.trajectory_at_sea_state_time.ix[index, quant_name] = float(value_at_position)

            if self.progressbar:
                PB_WIDGETS[-1] = progress_bar_message(cnt, n_rows)
                progress.update(cnt)
                sys.stdout.flush()

            self.n_valid_samples += 1

        message = "Done with interpolating"
        if self.progressbar:
            progress.finish()
            print(message)
        else:
            _logger.info(message)

    def interpolate_sea_states_to_trajectory(self):
        """
        Create a latitude/longitude data frame at the same (3h) interval of the netcdf file
        """

        # these are the columns which are going to be added to the trajectory
        _columns = [self.latitude_name,
                    self.longitude_name,
                    ]

        # create a new data frame which is going to hold the sampled data of the sea state  times
        # based on netcdf database. It as the lat/on columns but these are empty
        self.trajectory_at_sea_state_time = pd.DataFrame(index=self.sea_states_date_times,
                                                         columns=_columns)
        self.trajectory_at_sea_state_time.index.name = self.date_time_column_name

        # create a combined data frame of the trajectory_at_seastate time and monitor trajectory
        # with the distance
        df_inter = pd.concat(
            [self.trajectory_data_frame[_columns],
             self.trajectory_at_sea_state_time[_columns]])
        #
        # we have a combined data frame with the empty lat/lon at 3h interval and the real lat/lon
        # at 30m interval. Sort them such that got in the right order and then interpolate to
        # fill the gaps of the lat/lons which were missing from the 3h interval data frame
        df_inter.sort_index(inplace=True)
        try:
            df_inter.interpolate(inplace=True, method="time")
        except TypeError:
            _logger.warning("Nothin to interpolate here. ")
            return
        df_inter.dropna(inplace=True)

        # the concat data frame has double indexes in case the sea state time was equal to the
        # monitor time. We would like to use drop_duplicate to remove the double indices, however,
        # does only works on columns. Therefore, first set a normal counter as index, which creates
        # a new column with the datetimes (with the duplicates)  remove the duplicates
        # on this 'index' and then set is back as index

        # move the index to a column, now we have a normal index as 0, 1, 2, etc
        df_inter = df_inter.reset_index()
        # drop duplicates of the columns given by the date time column
        df_inter = df_inter.drop_duplicates(keep="last", subset=self.date_time_column_name)

        # put the date time column back as an index
        df_inter.set_index(self.date_time_column_name, inplace=True, drop=True)

        # this command selects only the times at the trajectory_sea_states again
        df_inter = df_inter.reindex(self.trajectory_at_sea_state_time.index).dropna()
        _logger.debug("DFINTER:\n{}".format(df_inter))

        # df_inter now has the same index as the trajectory, so we can copy the values to data
        # frame at the sea state times. In this way the lat/lon become available
        for col_name in df_inter.columns.values:
            self.trajectory_at_sea_state_time[col_name] = df_inter[col_name]

        self.trajectory_at_sea_state_time.dropna(how='all', inplace=True)

        # now add all the sea state fields to our trajectory at sea state time
        for col_name in list(self.sea_states_3d.data_vars.keys()):
            self.trajectory_at_sea_state_time[col_name] = np.nan

        # we have now a data frame with the same times as the netcdf data and a lat/lon column
        # base on the trajectory we have read. Now obtain the sea states for each time at
        # the lat/lon location by looping over all the 2d data planes with sea state
        self.interpolate_data_base_of_times()

        # remove any row with nans if still left
        self.trajectory_at_sea_state_time.dropna(how='all', inplace=True)

        if self.n_valid_samples == 0:
            _logger.warning("Could not find any valid samples. Probably the time ranges of the "
                            "trajectory does not overlap with the netcdf file")
            sys.exit(0)

        # now we can put back the trajectory at sea state times back to the original interval,
        # because eventually we want to write the new 30 min interval back to file concatenate
        # the monitored lat lon at 30 min interval
        trajectory_at_sample_times = pd.concat([self.trajectory_data_frame,
                                                self.trajectory_at_sea_state_time])
        trajectory_at_sample_times.sort_index(inplace=True)

        # this fill all the gaps
        trajectory_at_sample_times.interpolate(inplace=True, method="time")

        # here we drop the double date/times again. Do this by resetting the index, remove the
        # doubles and put the index back.
        trajectory_at_sample_times = trajectory_at_sample_times.reset_index()
        trajectory_at_sample_times.drop_duplicates(keep="first",
                                                   subset=self.date_time_column_name,
                                                   inplace=True)
        trajectory_at_sample_times.set_index(self.date_time_column_name, inplace=True, drop=True)

        # calculate the travel distance and heading from the new coordinates
        trajectory_at_sample_times = travel_distance_and_heading_from_coordinates(
            trajectory_at_sample_times,
            latitude_name=self.latitude_name,
            longitude_name=self.longitude_name,
            heading_name=self.heading_name)
        trajectory_at_sample_times[self.date_time_column_name] = trajectory_at_sample_times.index
        try:
            trajectory_at_sample_times = get_speed_from_distance_and_time(
                trajectory_at_sample_times)
        except IndexError:
            _logger.warning(
                "No speed could be determined. Most likely we have only one valid point")
            pass
        trajectory_at_sample_times.drop(self.date_time_column_name, axis=1, inplace=True)

        # set the same row as the original data set
        trajectory_at_sample_times = trajectory_at_sample_times.ix[self.trajectory_date_time_index]

        # finally ,replace the original trajectory data frame
        self.trajectory_data_frame = trajectory_at_sample_times

    def lat_lon_at_times_of_sea_states(self):
        """
        Get the lat/lons at the 3h forecast times based on the trajectory stored in the sea states
        """

        _logger.info("Map sea state time to trajectory for speed {}".format(self.speed))

        _columns = [self.latitude_name,
                    self.longitude_name,
                    self.travel_distance_name,
                    self.travel_time_name,
                    self.heading_name,
                    self.speed_name]

        # first copy the current import trajectory (which does not have date/time yet.
        # and calculate the travel distance
        trajectory_to_sample = self.trajectory_data_frame.reset_index(drop=True)

        trajectory_to_sample = travel_distance_and_heading_from_coordinates(
            trajectory_to_sample,
            latitude_name=self.latitude_name,
            longitude_name=self.longitude_name,
            heading_name=self.heading_name,
            travel_distance_name=self.travel_distance_name
        )

        # for the prediction we get the lat/lons with a assumed speed based on the average so far
        # we are going to create a trajectory
        trajectory_at_seastate_times = pd.DataFrame(index=self.sea_states_date_times,
                                                    columns=_columns)
        # calculate travel time and distance
        speed_in_knots = self.speed.to("knots").magnitude
        travel_time_in_hr = np.array(
            [time / pd.Timedelta('1 hour') for time in self.sea_states_delta_time])
        travel_distance_in_nm = speed_in_knots * travel_time_in_hr

        # import speed and time distance in new data frame
        trajectory_to_sample[self.speed_name] = speed_in_knots
        trajectory_at_seastate_times[self.speed_name] = speed_in_knots
        trajectory_at_seastate_times[self.travel_time_name] = travel_time_in_hr
        trajectory_at_seastate_times[self.travel_distance_name] = travel_distance_in_nm

        # set the travel distance as index of the dataframe
        trajectory_at_seastate_times.set_index(self.travel_distance_name, inplace=True, drop=True)
        trajectory_at_seastate_times.drop_duplicates(keep="last", inplace=True)

        # do the same for the trajectory which we want to sample
        trajectory_to_sample.set_index(self.travel_distance_name, inplace=True, drop=True)

        # now we need the latitude/longitudes at the travel distances. We have stored those in the
        # trajectory data frame, so combine the data frames and interpolate on the index
        # (the travel_distance).
        lat_lon_interp = pd.concat(
            [trajectory_at_seastate_times, trajectory_to_sample]).sort_index()
        # remove the duplicated distances
        lat_lon_interp = lat_lon_interp.groupby(lat_lon_interp.index).first()
        # interpolate all NaN
        lat_lon_interp.interpolate(method="index", inplace=True)

        # remove rows with nan in both the distance and the speed
        lat_lon_interp = lat_lon_interp[pd.notnull(lat_lon_interp.index)]
        lat_lon_interp = lat_lon_interp[pd.notnull(lat_lon_interp[self.speed_name])]

        # Now we have a data frame containing the lat/lon of both the forecast trajectory (which
        # are set to nan) and the voyage we want (as stored in the trajectory data base).
        # Select only the travel distance belonging to the original forecast distances
        lat_lon_interp = lat_lon_interp.reindex(trajectory_at_seastate_times.index)

        # copy the new lat/lon to the original forecast_trajectory
        for name in _columns:
            try:
                trajectory_at_seastate_times.loc[:, name] = lat_lon_interp[name]
            except KeyError:
                pass

        # put back the date time as index.
        trajectory_at_seastate_times[self.date_time_column_name] = self.sea_states_date_times
        trajectory_at_seastate_times.set_index(self.date_time_column_name, drop=False, inplace=True)

        trajectory_at_seastate_times = travel_distance_and_heading_from_coordinates(
            trajectory_at_seastate_times)
        try:
            trajectory_at_seastate_times = get_speed_from_distance_and_time(
                trajectory_at_seastate_times)
        except IndexError:
            # most likely we have only one data point. Impose a constant speed
            trajectory_at_seastate_times[self.speed_name] = self.speed.to("knots").magnitude
        else:
            # we are missing the first values of the row. Calculate them
            set_lat_lon_between_indices(trajectory_at_seastate_times, 0, 1,
                                        latitude_name=self.latitude_name,
                                        longitude_name=self.longitude_name,
                                        head_string=self.heading_name)

        trajectory_at_seastate_times.drop([self.date_time_column_name], inplace=True, axis=1)

        # copy the result to the trajectory data frame such we can continue
        self.trajectory_data_frame = trajectory_at_seastate_times

    def report_last_position_and_speed(self):
        """
        Extract the last location and speed from the data frame and write to screen
        """

        _date_times = pd.DatetimeIndex(self.trajectory_data_frame.index)
        try:
            last_latitude = self.trajectory_data_frame[self.latitude_name].values[-1]
            last_longitude = self.trajectory_data_frame[self.longitude_name].values[-1]
        except IndexError:
            print("Last Position (Lat, Lon): Warning: empty data frame")
        else:
            print("Last Position (Lat, Lon): ({:.2f}, {:.2f})".format(last_latitude,
                                                                      last_longitude))

        last_speed = None
        try:
            last_time = _date_times[-1]
            start_window = last_time - pd.Timedelta(self.average_window_length, "h")
            average_window = _date_times >= start_window
            speed = self.trajectory_data_frame.ix[average_window][self.speed_name]
            last_speed = speed.mean()
        except IndexError:
            print("WARNING: Could not get an final average speed of the last 10 h")
        finally:
            print("Last Speed: {}".format(set_default_dimension(last_speed, "knots")))

    def export_trajectory(self):
        """
        Export the results to a new data base
        """

        _logger.info("Writing data base to {}".format(self.output_file_name))

        # create a time mask such that we only store the data which has been interpolated
        _date_times = pd.DatetimeIndex(self.trajectory_data_frame.index)
        time_mask = (_date_times >= self.first_date_time) & (_date_times <= self.last_date_time)

        with pd.ExcelWriter(self.output_file_name) as writer:

            if self.data_sheets is not None:
                # always try to write the information sheet first. Note that we only have data sheet
                # in case we were reading the trajectory from excel. For kml trajectories we can
                # skip this
                _logger.debug("writing Information sheet")
                try:
                    info_sheet = self.data_sheets[self.information_sheet_name]
                except KeyError:
                    _logger.info("No Information sheet found.")
                else:
                    info_sheet.set_index("property", drop=True).to_excel(
                        writer, sheet_name=self.information_sheet_name)

            # write the sheet with the lat/lon information
            _logger.debug("writing general sheet")

            # select the time we have interpolated only
            self.trajectory_data_frame = self.trajectory_data_frame.ix[time_mask]

            # we need to convert the date/time index to normal strings, otherwise the excel writer
            # does not understand it.
            self.trajectory_data_frame.index = self.trajectory_data_frame.index.astype(str)

            # now write all the trajectory data to the general sheet name
            self.trajectory_data_frame.to_excel(writer, sheet_name=self.general_sheet_name)

            # finally write all the other sheets in case we are dealing with excel trajectories
            if self.data_sheets is not None:
                for key, db in self.data_sheets.items():
                    if key in (self.general_sheet_name, self.information_sheet_name):
                        continue
                    # al the remain sheet need to be written again. Make sure that the date/time
                    # index is a index, not a column, and select the same time range as the
                    # general sheet, including teh same time zone (
                    db.set_index([self.date_time_column_name], inplace=True, drop=True)
                    db = db.ix[time_mask]
                    db.index = db.index.astype(str)
                    db.to_excel(writer, sheet_name=key)


def _parse_the_command_line_arguments(args):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # parse the command line to set some options2
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    parser = argparse.ArgumentParser(description='Interpolate weather data along route',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # set the verbosity level command line arguments
    # mandatory arguments
    parser.add_argument("netcdf_input_file", action="store",
                        help="Netcdf file containing all sea states. The file can be generated  "
                             "with the gt_ftp_to_netcdf utility")
    parser.add_argument("--trajectory_input_file", action="store",
                        help="File containing the  coordinates of the trajectory along which we "
                             "want to interpolate the sea states of the netcdf file")
    parser.add_argument('--log_file_debug', help="Print lots of debugging statements to file",
                        action="store_const",
                        dest="log_level_file", const=logging.DEBUG)
    parser.add_argument('--log_file_verbose', help="Be verbose to file", action="store_const",
                        dest="log_level_file",
                        const=logging.INFO)
    parser.add_argument('--log_file_quiet', help="Be quiet: no output to file",
                        action="store_const",
                        dest="log_level_file", const=logging.WARNING)
    parser.add_argument("--write_log_to_file", action="store_true",
                        help="Write the logging information to file")
    parser.add_argument("--log_file_base", default="log", help="Default name of the logging output")
    parser.add_argument("--version", help="Show the current version", action="version",
                        version="{}\nPart of hmc_tools version {}".format(
                            os.path.basename(__file__), __version__))
    parser.add_argument("--latitude",
                        help="Latitude of fixed location where we want to get the sea states, or, "
                             "in case we have also given a kml file, from which we want to start "
                             "interpolating along the route. Can be given in both decimal and "
                             "h/d/m notation. For the latter, use underscores to separate the "
                             "degrees/minutes, ie.  5_52_58.9_N",
                        )
    parser.add_argument("--longitude",
                        help="Longitude of fixed location where we want to get the sea states, or, "
                             "in case we have also given a kml file, from which we want to start "
                             "interpolating along the route. Can be given in both decimal and "
                             "h/d/m notation. For the latter, use underscores to separate the "
                             "degrees/minutes, ie.  \"65 52 58.9 W\"",
                        )
    parser.add_argument("--format_lat_lon", default="d% %m% %S% %H",
                        help="Latitude/Longitude format in case we are not passing floats")
    parser.add_argument("--replace_gps_zero_with_nan",
                        help="In case GPS values of 0,0 are stored, the gps system was switched "
                             "off. Replace the zeros with Nan and interpolate to fill the gap",
                        action="store_true", default=True)
    parser.add_argument("--no_replace_gps_zero_with_nan",
                        help="Do not replace the 0,0 values with Nan ",
                        action="store_false", dest="replace_gps_zero_with_nan")
    parser.add_argument("--clip_speed",
                        help="If we have erroneous gps values this will lead to a peak in the "
                             "velocity. In case the clip_speed is larger than 0, all rows with a "
                             "velocity larger than 'clip_speed' (in kn) are removed", default=100.0)
    parser.add_argument("--speed_name", action="store", default="speed_sustained",
                        help="Name of the speed column")
    parser.add_argument("--travel_distance_name", action="store", default="travel_distance",
                        help="Name of the travel distance column")
    parser.add_argument("--file_id", action="store", help="A file ID added to the output file name")
    # options
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level", const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level", const=logging.WARNING)
    parser.add_argument('--progressbar', help="Show a progress bar", action="store_true")
    parser.add_argument("--lat_lon_division", action="store",
                        help="Sometimes lat/lons are multiplied by 100. You can "
                             "correct it if you want", default=None, type=float)
    parser.add_argument("--latitude_name", action="store",
                        help="Name of the latitude in the data base",
                        default="GPS_LATITUDE")
    parser.add_argument("--heading_name", action="store",
                        help="Name of the heading in the data base",
                        default="HEADING")
    parser.add_argument("--longitude_name", action="store",
                        help="Name of the latitude in the data base",
                        default="GPS_LONGITUDE")
    parser.add_argument("--start_way_point",
                        help="Start reading the way point file from this way point", type=int,
                        default=None)
    parser.add_argument("--number_of_sample_points",
                        help="Number of point to use for sampling the waypoints file",
                        default=2000, type=int)
    parser.add_argument("--speed_in_knots", default="6.0",
                        help="The speed of the vessel in knots. If the monitoring data is "
                             "available this will be taken", type=float)
    parser.add_argument("--start_distance",
                        help="Location of the Balder at the start of the forecast",
                        default=0.0, type=float)
    parser.add_argument("--date_time_start", action="store",
                        help="Start date of the interpolation in the database."
                             "If none is given, we will try to get the date time "
                             "start from the database file name.")
    parser.add_argument("--date_time_end", action="store",
                        help="End date of the interpolation in the database."
                             "If none is given, we will try to get the date time "
                             "end from the database file name.")
    parser.add_argument("--max_relative_time", action="store", type=int,
                        help="Maximum time you want to interpolate in hours from the start time."
                        )
    parser.add_argument("--output_file_name", action="store",
                        help="Name of the output trajectory file. If none is given it will be "
                             "based on the trajectory input file with the datestring attached")
    parser.add_argument("--base_name_output_file", action="store",
                        help="Base of the output trajectory file. If none is given it will be "
                             "based on the trajectory input file with the datestring attached to "
                             "it ", default="route")
    parser.add_argument('--date_time_column_name',
                        help="Name of the DateTime column in the excel data base",
                        default="DateTime")
    parser.add_argument('--general_sheet_name',
                        help="Name of the data sheet with the lat lon data ", default="General")

    # parse the command line
    parsed_arguments = parser.parse_args(args)

    return parsed_arguments, parser


def set_lat_lon_between_indices(trajectory_db, index1, index2, latitude_name="latitude",
                                longitude_name="longitude", head_string="heading",
                                travel_distance_string="travel_distance"):
    """
    Set the latitude/longitude at index1 based on the latitude/longitude at index2 by taking
    the heading and distance between the point

    Parameters
    ----------
    trajectory_db: DataFrame
        Database containing the values
    index1: int
        Index of the first point
    index2: int
        Index of the second point
    latitude_name: str, optional
        Name of the latitude
    longitude_name: str, optional
        Name of the longitude
    head_string: str, optional
        Name of the heading
    travel_distance_string: st, optionalr
        Name of the travel distance column
    """

    # get the heading and latitude longitude at the index2 from the data base
    heading = trajectory_db[head_string].values[index2]
    latitude = trajectory_db[latitude_name].values[index2]
    longitude = trajectory_db[longitude_name].values[index2]

    # set the heading the same
    trajectory_db.ix[index1, head_string] = heading

    # calculate the latitude and longitude at the other location based on the heading and distance
    loc1_pos = geopy.Point(latitude, longitude)
    loc1_dist_in_nm = Q_(trajectory_db[travel_distance_string].values[index2], "nautical_mile")
    loc1_dist_in_km = loc1_dist_in_nm.to("km").magnitude
    loc1_bearing = heading + 180  # add 180 because we are traveling back
    destination = VincentyDistance(kilometers=loc1_dist_in_km).destination(loc1_pos, loc1_bearing)
    trajectory_db.ix[index1, latitude_name] = destination.latitude
    trajectory_db.ix[index1, longitude_name] = destination.longitude


def setup_logging(write_log_to_file=False,
                  log_file_base="log",
                  log_level_file=logging.INFO,
                  log_level=None,
                  progress_bar=False,
                  ):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Initialise the logging system
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    if write_log_to_file:
        # http://stackoverflow.com/questions/29087297/
        # is-there-a-way-to-change-the-filemode-for-a-logger-object-that-is-not-configured
        sys.stderr = open(log_file_base + ".err", 'w')
    else:
        log_file_base = None

    _logger = create_logger(file_log_level=log_level_file,
                            console_log_level=log_level,
                            log_file=log_file_base)

    if progress_bar:
        # switch of all logging because we are showing the progress bar via the print statement
        # logger.disabled = True
        # logger.disabled = True
        # logger.setLevel(logging.CRITICAL)
        for handle in _logger.handlers:
            try:
                getattr(handle, "baseFilename")
            except AttributeError:
                # this is the stream handle because we get an AtrributeError. Set it to critical
                handle.setLevel(logging.CRITICAL)

    _logger.info("{:10s}: {}".format("Running", sys.argv))
    _logger.info("{:10s}: {}".format("Version", __version__))
    _logger.info("{:10s}: {}".format("Directory", os.getcwd()))


def main(args_in):
    args, parser = _parse_the_command_line_arguments(args_in)

    setup_logging(log_level=args.log_level, progress_bar=args.progressbar)

    start_time = pd.to_datetime("now")
    print_banner(
        "Start {} version {} at {}".format(os.path.split(__file__)[1], __version__, start_time))

    if args.latitude is not None and args.longitude is not None:
        # turn the lat/lon given at the command line into a latlon object. Replace _ with spaces
        try:
            fixed_coordinates = LatLon(Latitude(float(args.latitude)),
                                       Longitude(float(args.longitude))
                                       )
        except ValueError:
            # if we could read the latitude and longitude as float, try to convert if with the
            # string2latlon function using the format described by format_lat_lon argument
            fixed_coordinates = string2latlon(args.latitude, args.longitude,
                                              args.format_lat_lon)
    else:
        fixed_coordinates = None

    speed = set_default_dimension(args.speed_in_knots, "knots")
    start_distance = set_default_dimension(args.start_distance, "nautical_miles")

    RouteInterpolator(
        netcdf_input_file=args.netcdf_input_file,
        trajectory_input_file=args.trajectory_input_file,
        coordinates=fixed_coordinates,
        replace_gps_zero_with_nan=args.replace_gps_zero_with_nan,
        clip_speed=args.clip_speed,
        speed_name=args.speed_name,
        travel_distance_name=args.travel_distance_name,
        file_id=args.file_id,
        lat_lon_division=args.lat_lon_division,
        progressbar=args.progressbar,
        heading_name=args.heading_name,
        latitude_name=args.latitude_name,
        longitude_name=args.longitude_name,
        start_way_point=args.start_way_point,
        number_of_sample_points=args.number_of_sample_points,
        speed=speed,
        start_distance=start_distance,
        date_time_start=args.date_time_start,
        date_time_end=args.date_time_end,
        max_relative_time=args.max_relative_time,
        output_file_name=args.output_file_name,
        base_name_output_file=args.base_name_output_file,
        date_time_column_name=args.date_time_column_name,
        general_sheet_name=args.general_sheet_name,
        write_to_file=True
    )


def _run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == '__main__':
    _run()
