#!/bin/sh
"""
Obtain all the grib file from the NOAA FTP side or the HMC IS monitoring net work drive and store
them into a net file as a 3D netcdf data array
"""
from __future__ import print_function

import argparse
# use ftplib as request does not work with ftp
import ftplib
import logging
import os
import re
import sys
import time
from collections import OrderedDict

import netCDF4 as nc
import numpy as np
import pandas as pd
import progressbar as pb
import xarray as xr
from hmc_utils.file_and_directory import (make_directory)
from hmc_utils.misc import (create_logger, get_logger, print_banner,
                            set_value_if_valid, clear_path, read_settings_file)
from mpl_toolkits.basemap import shiftgrid
from osgeo import gdal

from hmc_tools import __version__

# set up progress bar properties
PB_WIDGETS = [pb.Percentage(), ' ', pb.Bar(marker='.', left='[', right=']'), ""]


def get_unique_element_name(band_meta_data, show_key_value_of_fields=False):
    """
    Get the unique name of this layer meta data

    Parameters
    ----------
    band_meta_data: meta_data
        meta data of the current band name
    show_key_value_of_fields: bool
        For debugging purpose: if true, show all the fields

    Returns
    -------
    tuple
        (key_name, key_unit)
    """

    logger = get_logger(__name__)
    element = band_meta_data["GRIB_ELEMENT"]
    element_cnt = 1
    short_name = band_meta_data["GRIB_SHORT_NAME"]
    m = re.match("^\d+", short_name)
    if bool(m):
        element_cnt = m.group(0)
    key_name = element + element_cnt

    # extract the units of this quantity
    key_unit = band_meta_data["GRIB_UNIT"]

    comment = band_meta_data["GRIB_COMMENT"]
    logger.debug(" ============= {:10s} {:} {:20}".format(element, element_cnt, comment))

    if show_key_value_of_fields:
        for cnt, (key, value) in enumerate(band_meta_data.items()):
            logger.debug(" {:3d} {:10s} {:} ".format(cnt, key, value))

    return key_name, key_unit


def download_grib_file(ftp_side, ftp_file, out_file):
    """
    Down load the grib file from the NOAA side

    Parameters
    ----------
    ftp_side: FTP
        Link to the ftp side which should be open already
    ftp_file: str
        Name of the ftp file to download
    out_file:
        Name of the output file
    """
    logger = get_logger(__name__)

    logger.info("Downloading file {} ".format(ftp_file))
    ftp_side.retrbinary("RETR " + ftp_file, open(out_file, 'wb').write)


def progress_bar_message(file_count, file_total_count):
    """The message to show in the progress bar"""
    return "Downloaded file {:d} of {:d}".format(file_count + 1, file_total_count)


def _parse_the_command_line_arguments(args):
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # parse the command line to set some options2
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    # sys.argv = clear_argument_list(sys.argv)

    parser = argparse.ArgumentParser(
        description='Tool to download NOAA sea state for cast from ftp side and turn all'
                    'files into a single netcdf file',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--version", help="Show the current version", action="version",
                        version="{}\nPart of hmc_tools version {}".format(
                            os.path.basename(__file__), __version__))

    # set the verbosity level command line arguments
    parser.add_argument('-d', '--debug', help="Print lots of debugging statements",
                        action="store_const", dest="log_level", const=logging.DEBUG,
                        default=logging.INFO)
    parser.add_argument('-v', '--verbose', help="Be verbose", action="store_const",
                        dest="log_level",
                        const=logging.INFO)
    parser.add_argument('-q', '--quiet', help="Be quiet: no output", action="store_const",
                        dest="log_level",
                        const=logging.WARNING)
    parser.add_argument('--log_file_debug', help="Print lots of debugging statements to file",
                        action="store_const",
                        dest="log_level_file", const=logging.DEBUG)
    parser.add_argument('--log_file_verbose', help="Be verbose to file", action="store_const",
                        dest="log_level_file",
                        const=logging.INFO)
    parser.add_argument('--log_file_quiet', help="Be quiet: no output to file",
                        action="store_const",
                        dest="log_level_file", const=logging.WARNING)
    parser.add_argument("--write_log_to_file", action="store_true",
                        help="Write the logging information to file")
    parser.add_argument("--log_file_base", default="log", help="Default name of the logging output")
    parser.add_argument('--compress_data',
                        help="Use zlib data compression on all the variable data",
                        action="store_true", default=True)
    parser.add_argument('--no_compress_data',
                        help="Do not use zlib data compression on the variable data",
                        action="store_false", dest="compress_data")
    parser.add_argument('--progressbar', help="Show a progress bar", action="store_true")
    parser.add_argument('--reset_netcdf_global', help="Reset the netcdf data base",
                        action="store_true")
    parser.add_argument('--force_grib_read', help="Force to read the grib files",
                        action="store_true")
    parser.add_argument('--force_ftp_download', help="Force to download the ftp files",
                        action="store_true")
    parser.add_argument('--internet', help="Internet connection is available", action="store_true",
                        default=True,
                        dest="internet")
    parser.add_argument('--no_internet', help="No internet connection is available",
                        action="store_false",
                        dest="internet")
    parser.add_argument('--write_selection', help="Write the selection in space to file",
                        action="store_true")
    parser.add_argument("--max_grib_files", help="Maximum number of grib files to import",
                        action="store", type=int)
    parser.add_argument("--ftp_directory", help="Name of the directory on the FTP server where the "
                                                "data is located",
                        default="pub/data/nccf/com/wave/prod/")
    parser.add_argument("--ftp_server_name", help="Name of the ftp server where the NOAA files"
                                                  "are located", default="ftpprd.ncep.noaa.gov")
    parser.add_argument("--network_directory",
                        default="\\\\alecto\\MAEdata\\Wavedata\\NOAAwavewatch3_forecast_grib2",
                        help="Name of the network_directory where the Daily files downloaded by IS "
                             "are stored")
    parser.add_argument("--directory_base_name",
                        help="Base name of all the directories with the forecast data files. "
                             "The total directory name also contains the date: e.g "
                             "base_name.20160921", default="multi_1.")
    parser.add_argument("--grib_file_base_name", help="Base name of the grib files to download ",
                        default="multi_1.glo_30m")
    parser.add_argument("--grib_file_extension", help="Extension of the grib files to download ",
                        default=".grib2")
    parser.add_argument('--stop_after_failure',
                        help="As soon as the first grib file cannot be downloaded, stop "
                             "the process", action="store_true", default=True)
    parser.add_argument('--continue_after_failure',
                        help="Continue with downloading, even if a failure occurs",
                        action="store_false", dest="stop_after_failure")
    parser.add_argument('--hmc_grib_files', action="store_true",
                        help="In stead of downloading the grib files from the NOAA server you can "
                             "also obtained all the grib files as downloaded by HMC IS to the"
                             "network_directory (see the --network_directory option). If you set"
                             "this flag the netcdf file is created from the hmc downloads",
                        )
    parser.add_argument("--working_directory",
                        help="Name of the directory where all files are downloaded.",
                        default=".")

    parser.add_argument("--temporary_output_directory",
                        help="Store all the downloaded grib files into this directory",
                        default="grib_out_directory")
    parser.add_argument("--one_output_directory",
                        help="Store all the downloaded grib files into one directory",
                        action="store_true")
    parser.add_argument("--prefix_format", action="store", default="_1.%Y%m%d.",
                        help="The grib file stored into one directory (when one_output_directory "
                             "is set) need a date prefix. Give here a data/time prefix format to "
                             "use base on the current data time stamp")
    parser.add_argument("--work_directory",
                        help="Store all converted netcdf files into this directory",
                        default=".")

    parser.add_argument("--netcdf_global_output_file_name",
                        help="Name of the netcdf file of the now casts")

    parser.add_argument("--test", help="Just show the steps to process. Do not do anything yet.",
                        action="store_true")

    parser.add_argument("--date_start", help="Start date to import, given as eg 20160921",
                        default="today")
    parser.add_argument("--date_end",
                        help="End date time to import. If not given, same as start date",
                        default=None)
    parser.add_argument("--time_start",
                        help="Start time (in hours) to import. Must be multiple of time step",
                        default=0, type=int)
    parser.add_argument("--time_step", help="Number of hours to skip", default=6, type=int)
    parser.add_argument("--time_end", help="End time to import. If not given, same as start",
                        default=None, type=int)

    parser.add_argument("--configuration_file",
                        help="Name of the configuration settings of this utility",
                        default="gt_settings.yaml")

    parser.add_argument("--fc_start", help="Start of forecast times to download", action="store",
                        type=int, default=0)
    parser.add_argument("--fc_end", help="End of forecast times to download", action="store",
                        type=int, default=180)
    parser.add_argument("--fc_delta", help="Skip time of the forecast times to download",
                        action="store", type=int,
                        default=3)

    # parse the command line
    parsed_arguments = parser.parse_args(args)

    return parsed_arguments, parser


def default_settings():
    """
    Default settings for the ftp script
    :return: dictionary with all the aliases for the field names in the grib files
    """
    settings = OrderedDict(
        GDAL_ALIASES=OrderedDict(
            HTSGW1="Hs_tot",
            PERPW1="Tp_tot",
            DIRPW1="direction_wave_tot",
            WVHGT1="Hs_wind",
            WVPER1="Tp_wind",
            WVDIR1="direction_wave_wind",
            SWELL1="Hs_swell",
            SWELL2="Hs_swell2",
            SWPER1="Tp_swell",
            SWPER2="Tp_swell2",
            SWDIR1="direction_wave_swell",
            SWDIR2="direction_wave_swell2",
            WIND1="speed_wind",
            WDIR1="direction_wind",
            UGRD1="u_wind",
            VGRD1="v_wind",
        ),
        selection=OrderedDict(
            write_selection=False,
            low_left_corner_latitude=20,
            up_right_corner_latitude=60,
            low_left_corner_longitude=-20,
            up_right_corner_longitude=10
        )
    )

    return settings


def main(args):
    args, parser = _parse_the_command_line_arguments(args)

    if args.write_log_to_file:
        # http://stackoverflow.com/questions/29087297/
        # is-there-a-way-to-change-the-filemode-for-a-logger-object-that-is-not-configured
        log_file_base = args.log_file_base
        sys.stderr = open(log_file_base + ".err", 'w')
    else:
        log_file_base = None

    logger = create_logger(file_log_level=args.log_level_file,
                           console_log_level=args.log_level,
                           log_file=log_file_base,
                           file_log_format_long=False)

    # store the starting time and print a banner to the output
    start_time = pd.to_datetime("now")
    print_banner("Start {} version {} with {} at {}".format(os.path.split(__file__)[1], __version__,
                                                            args.configuration_file, start_time))

    if args.progressbar:
        # switch of all log messages if we are showing progress bars
        logger.setLevel(logging.CRITICAL)

    if args.test and logger.getEffectiveLevel() == logging.CRITICAL:
        # testing phase implies info output
        logger.setLevel(logging.INFO)

    # try to open the configuration file given on the commandline at several locations. If none is
    # found, load the default settings
    try:
        settings = read_settings_file(args.configuration_file)
    except AssertionError:
        logger.info("Taking default configuration settings".format(args.configuration_file))
        settings = default_settings()

    logger.debug(settings)

    try:
        gdal_aliases = settings["GDAL_ALIASES"]
    except KeyError as err:
        logger.warning(
            "Required key field GDAL_ALIASES not available in settings configuration file")
        parser.error(err)

    if args.date_start == "today":
        args.date_start = start_time.strftime("%Y%m%d")

    if args.hmc_grib_files:
        # create all settings required to download from the IS server
        if args.netcdf_global_output_file_name is None:
            args.netcdf_global_output_file_name = "nowcast_database.nc"

        # these settings are needed to download the now cast from the hmc server
        args.fc_start = 0
        args.fc_end = 3
        args.time_start = 0
        args.time_end = 18
        args.internet = False
        args.temporary_output_directory = args.network_directory
        args.one_output_directory = True
        if args.date_end is None:
            # if the end date is not given download until today
            args.date_end = start_time.strftime("%Y%m%d")

    # if date end is not given, take same as date start
    if args.date_end is None:
        args.date_end = args.date_start

    # if time end is not given, take same as time start
    if args.time_end is None:
        # add one hour to the end time to get a closed range
        args.time_end = args.time_start

    # create the list of dates to import
    date_range = pd.date_range(args.date_start, args.date_end, freq="D")
    logger.debug("Import date range {}".format(date_range))

    # create the list of times to import
    time_range = list(range(args.time_start, args.time_end + 1, args.time_step))
    logger.debug("Import time range {}".format(time_range))

    # create a list of forecast times to import.
    fc_times_to_import = list(range(args.fc_start, args.fc_end + 1, args.fc_delta))
    max_files = len(fc_times_to_import)
    if args.max_grib_files is not None:
        max_files = min(args.max_grib_files, max_files)

    logger.debug("Maximum number of grib file to import {}".format(max_files))

    logger.debug("Created forecast time list {}".format(fc_times_to_import))

    # the global netcdf file has the same name as the global output name with the extension 'nc'
    if args.netcdf_global_output_file_name is not None:
        # create the total file name to the netcfd file captaining the global output names
        nowcast_sea_states_file_name = "/".join(
            [args.work_directory, args.netcdf_global_output_file_name])
        if os.path.exists(nowcast_sea_states_file_name) and not args.reset_netcdf_global:
            logger.info("Appending to netcdf data base for now-cast data arrays {}"
                        "".format(args.netcdf_global_output_file_name))
            nowcast_sea_states = nc.Dataset(nowcast_sea_states_file_name, mode='a')
            logger.debug("nowcast sea states \n{}".format(nowcast_sea_states))

            logger.debug("sea states\n{}".format(nowcast_sea_states))

            # get the time array for later reference
            nowcast_times = nowcast_sea_states.variables["time"]
            # nowcast_times = nc.num2date(nc_time[:], units="hours since 0001-01-01 00:00:00",
            #                              calendar="gregorian")
            logger.debug("nowcast times 2\n{}".format(nowcast_times))
        else:
            logger.info(
                "Creating new now-cast data array {}".format(args.netcdf_global_output_file_name))
            make_directory(args.work_directory)
            nowcast_sea_states = nc.Dataset(nowcast_sea_states_file_name, mode='w',
                                            format="NETCDF4")

            # the time data base was empty, so initialise it
            logger.info("Initialising database with an empty time coordinate")
            nowcast_sea_states.createDimension("time", None)
            nowcast_times = nowcast_sea_states.createVariable("time", np.float64, ('time',),
                                                              zlib=args.compress_data)
            nowcast_times.units = "hours since 0001-01-01 00:00:00"
            nowcast_times.calendar = "gregorian"
    else:
        # set the reference to the nowcast data base to none.
        nowcast_sea_states = None

    # create the base name of the grib file directory
    out_base_name_global = args.grib_file_base_name

    first_time_stamp = None

    stop_downloading = False

    ftp_side = None

    # loop over the dates to import and import the files
    for date_to_import in date_range:
        if stop_downloading:
            logger.info("Quit forced.  Finalizing")
            break

        logger.info("Importing  from date {}".format(date_to_import))

        # turn the date/time object into a string carrying only the date in YYMMDD format
        date_string = date_to_import.strftime("%Y%m%d")
        dir_base_name_this_date = args.directory_base_name + date_string

        # all forecast are in one directory of the ftp server. Loop over this directory
        if args.internet:
            if ftp_side is None:
                # first time we need to login at the server
                ftp_side = ftplib.FTP(args.ftp_server_name)
                ftp_side.login("anonymous", "")
                try:
                    ftp_side.cwd(args.ftp_directory)
                except ftplib.error_perm:
                    raise ValueError("Could not open directory {}. Something it wrong "
                                     "because this is the data root dir".format(args.ftp_directory))

            # now try to move to the current date
            try:
                ftp_side.cwd(dir_base_name_this_date)
            except ftplib.error_perm:
                logger.info("Could not open the current directory {}. Go the the "
                            "next".format(dir_base_name_this_date))
                continue

        # loop over the start time of the current forecast.
        for time_to_import in time_range:
            if stop_downloading:
                logger.info("Quit forced.  Stop downloading any further time steps")
                break

            # create a time stamp of the current date/time. This time stamp refers to the start of
            # the forecast
            date_time_import = "{}T{:02d}{:02}{:02}".format(date_string, time_to_import, 0, 0)
            time_stamp = pd.Timestamp(date_time_import)

            if first_time_stamp is None:
                first_time_stamp = time_stamp

            # create the base name for this time stamp
            out_base_name = out_base_name_global + "_{}".format(date_time_import)

            # clean up the path
            out_base_name = clear_path(out_base_name)
            # remove those nasty dots in the directory names
            out_base_name = re.sub("\.", "_", out_base_name)

            temp_out_dir_wrt_work_dir = args.temporary_output_directory

            # build the output file name where to save all the forecast grib files of this date/time
            if args.one_output_directory:
                # all grib file in one output directory
                out_directory = temp_out_dir_wrt_work_dir
            else:
                # the grib files of all forecast in a separate directory
                out_directory = "/".join([temp_out_dir_wrt_work_dir, out_base_name])
            logger.debug("Creating directory {}".format(out_directory))
            if not args.test:
                # net cdf files are stored in the output directory of this script
                make_directory(args.work_directory)

                # grib files are stored in the temporary output directory (as we can trow them away)
                # + in a directory per data/time at which all the forecast files are downloaded
                # (up to 180 hours in total)
                make_directory(temp_out_dir_wrt_work_dir)
                if not args.one_output_directory:
                    # we want to download the forecast files in one directory per date/time, so
                    # create a separate one
                    make_directory(out_directory)

            if nowcast_sea_states is None:
                # the netcdf file has the same name as the output directory with the extension 'nc'
                # this net cdf file just contains all the forecast times for the current time_stamp
                netcdf_out_file = "/".join([args.work_directory, out_base_name]) + ".nc"
                logger.debug("Writing grib files to netcdf file {}".format(netcdf_out_file))

            # build the file base name of the current forecast file to download belonging to this
            # date/time
            file_base_name_this_time = args.grib_file_base_name + ".t{:02d}z".format(time_to_import)

            # this dictionary is going to hold all the data per sea state per forecast time
            fc_sea_states = OrderedDict()
            fc_times = list()
            fc_times_total = list()

            # set up progress bar for this series of forecast times
            if args.progressbar:
                print_banner(
                    "Download {} forecast files date: {} / time: {}".format(max_files, date_string,
                                                                            time_to_import),
                    to_stdout=True)
                progress = pb.ProgressBar(widgets=PB_WIDGETS, maxval=max_files,
                                          fd=sys.stdout).start()
                PB_WIDGETS[-1] = progress_bar_message(0, max_files)
                progress.update(0)
                sys.stdout.flush()

            # loop over the forecast times starting from zero and with step fc_delta
            # per time, load (download or import) the gdal data, and store to disk per quantity
            # in case nowcast_sea_state is set (not None), it means we are going to add time steps
            # to the total nowcast data base if they are not in yet, so always process this loop
            # over the forecast time in case nowcast_sea_states is None, we have the purpose to
            # generate a netcdf file per forecast series so only do this if it does not yet exist
            # or if force_grib_read is true
            coordinates_latitude = None
            coordinates_longitude = None
            if nowcast_sea_states is not None or (
                    args.force_grib_read or not os.path.exists(netcdf_out_file)):
                logger.debug("Start importing time steps {}".format(fc_times_to_import))
                for cnt, fc_time in enumerate(fc_times_to_import):

                    # check if we did not exceed the maximum requested files
                    if cnt == max_files or stop_downloading:
                        break

                    # create a time stamp of the total time, i.e the start of the forecast + the
                    # number of forecast hour
                    time_stamp_total = time_stamp + pd.Timedelta(fc_time, 'h')
                    fc_times_total.append(time_stamp_total)

                    if nowcast_sea_states is not None:
                        # check if we do not already have this time step
                        logger.debug("Current nowcast times:\n{}".format(nowcast_times))
                        logger.debug("time stamp total:\n{}".format(time_stamp_total))

                        # turn the total time stamp into a hours since 1/1/1 value, which is used
                        # for the netcdf file use the to_datetime function to turn the pandas
                        # timestamp into a numpy datetime
                        hours_since = nc.date2num(pd.to_datetime(time_stamp_total),
                                                  units=nowcast_sea_states.variables["time"].units,
                                                  calendar=nowcast_sea_states.variables[
                                                      "time"].calendar
                                                  )
                        logger.debug("time_stamp= {} total {}  ({}) hours_since: {}"
                                     "".format(time_stamp, time_stamp_total, type(time_stamp_total),
                                               hours_since))
                        logger.debug("Check time: {}".format(
                            nc.num2date(hours_since,
                                        units=nowcast_sea_states.variables["time"].units,
                                        calendar=nowcast_sea_states.variables["time"].calendar
                                        )))

                        logger.debug("Check if {} in {}".format(hours_since, nowcast_times[:]))
                        if hours_since in nowcast_times[:]:
                            logger.debug("Time {} ({}) was already in data base. Skipping to next"
                                         "".format(time_stamp_total, hours_since))
                            logger.info(
                                "Time {} already in data base. Skipping".format(time_stamp_total))
                            continue
                        else:
                            logger.debug(
                                "Time {} ({}) not yet in data base. Getting it from the grib"
                                "".format(time_stamp_total, hours_since))

                    logger.debug("Total time stamp {}".format(time_stamp_total))
                    logger.info("Importing time  {}".format(time_stamp_total))

                    # file the output file name of the current forecast time
                    file_base = file_base_name_this_time + ".f{:03d}".format(fc_time)
                    trajectory_file_name = file_base + args.grib_file_extension

                    if args.one_output_directory:
                        # if all fore cast file go into one directory prepend the file name with
                        # the date/time
                        prefix = time_stamp.strftime(args.prefix_format)
                        file_base = prefix + file_base

                    # create full file name to the output directory
                    out_file_base = "/".join([out_directory, file_base])
                    out_file = out_file_base + args.grib_file_extension

                    # only try to download the file if we have internet.
                    if args.internet:
                        # we have internet, check if we did not download the file. If so: skip it
                        # (unless force=True)
                        if args.force_ftp_download or not os.path.exists(out_file):
                            logger.info("Start importing {}".format(trajectory_file_name))
                            if args.test:
                                # we are testing only, so continue
                                continue
                            # download both the grib file + the grib.idx file with the same name
                            for extension in ["", ".idx"]:
                                try:
                                    download_grib_file(ftp_side, trajectory_file_name + extension,
                                                       out_file + extension)
                                except ftplib.error_perm as err:
                                    if extension != ".idx":
                                        logger.info("Failed url download: {}".format(err))
                                        if args.stop_after_failure:
                                            logger.info("Quit downloading any further files")
                                            # set cnt to maximum number of file to force a quit
                                            cnt = max_files
                                            stop_downloading = True
                                    # we did not force a stop here, so just continue to the next
                                    # file to download
                                    continue
                        else:
                            logger.info("File {} already imported. Skipping".format(out_file))

                    # now we have downloaded the grib file from the ftp side (either now or in a
                    # previous run) read the grib file into the gdal_object and extract the data
                    logger.info("Reading grib file {}".format(out_file))
                    gdal_object = gdal.Open(out_file, gdal.GA_ReadOnly)

                    try:
                        # get info from the total object. Not important because it is empty
                        meta_data = gdal_object.GetMetadata()
                    except AttributeError:
                        logger.warning("Failed opening the gdal file. Skip skipping to the next")
                        continue

                    logger.debug("Meta data : {}".format(meta_data))
                    logger.debug("gdal : {}".format(gdal_object.GetDescription()))

                    # RasterX and Y are properties of the gdal_object: it contains the the size of
                    # the domain
                    number_of_columns = gdal_object.RasterXSize
                    number_of_rows = gdal_object.RasterYSize

                    # GeoTransform returns an array with the following data
                    # top left x, pixel width, rotation, top left y, rotation, pixel height
                    # rotation=0 means north up
                    # notice pixel height is negative because it's measured from top!
                    geo_transform = gdal_object.GetGeoTransform()
                    logger.debug("geo transform: {}".format(geo_transform))
                    map_top_left_x = geo_transform[0]  # top left x
                    map_top_left_y = geo_transform[3]  # top left y
                    map_pixel_width = geo_transform[1]  # delta x  (west-east resolution)
                    map_pixel_height = geo_transform[
                        5]  # delta y (north-south resolution, negative because downward)
                    map_top_right_x = map_top_left_x + map_pixel_width * number_of_columns
                    map_bot_left_y = map_top_left_y + map_pixel_height * number_of_rows

                    # create the axis along the latitude and longitude used later for the xarray
                    # coordinate system
                    coordinates_longitude = np.linspace(map_top_left_x, map_top_right_x,
                                                        number_of_columns,
                                                        endpoint=False)
                    # note that the latitudes are stored from top to bottom
                    coordinates_latitude = np.linspace(map_top_left_y, map_bot_left_y,
                                                       number_of_rows, endpoint=False)

                    # loop over the bands and add the 2D data of all data in a list. For each scale
                    # (Hs, Tp, etc) an xarray DataArray is created (yielding 2D data arrays for each
                    # time). The data array for all forecast time are stored in the fc_sea_states
                    # dictionary (where each key is a list of time steps for this property) at the
                    # end, all the time steps are turned in a 3D data array, one for each property
                    forecast_time = None
                    for band in range(gdal_object.RasterCount):
                        # the band counter is one-based!
                        band += 1

                        # get the current layer from this time step
                        src_band = gdal_object.GetRasterBand(band)

                        # extract the info for this data band
                        band_meta_data = src_band.GetMetadata()

                        # get the unit and scale of the current scalar
                        unit_type = src_band.GetUnitType()
                        scale = src_band.GetScale()
                        logger.debug("unit type = {}, scale = {}".format(unit_type, scale))

                        if forecast_time is None:
                            # only set the forecast time based on the first record
                            # note that you can not add this to the attributes as the forecast time
                            # is different for each slice in time of the 3D data array
                            forecast_time = band_meta_data["GRIB_FORECAST_SECONDS"]

                        # get the unique key name referring to this data type
                        element_name, element_unit = get_unique_element_name(band_meta_data)

                        # create the new key name which is a bit more convenient. The aliases for
                        # all names are stored in the GDAL_ALIASES dictionary. In this way we
                        # have names corresponding to the rest of the fms software
                        try:
                            new_key_name = gdal_aliases[element_name]
                        except KeyError as err:
                            logger.warning(
                                "No alias for {} defined. Using GDAL name".format(element_name))
                            new_key_name = element_name

                        values = src_band.ReadAsArray()
                        # values_cyclic, lon_cyclic = addcyclic(values, coordinates_longitude)

                        # get all the data into a xarray 2D array
                        logger.debug("Creating data array: {}".format(new_key_name))
                        data = xr.DataArray(values,
                                            coords={"latitude": coordinates_latitude,
                                                    "longitude": coordinates_longitude},
                                            dims=["latitude", "longitude"],
                                            name=new_key_name,
                                            attrs={
                                                "units": element_unit,
                                                "scale_factor": scale,
                                                "time_stamp": str(time_stamp)
                                            },
                                            encoding={
                                                "units": element_unit,
                                                "calendar": "gregorian",
                                            }
                                            )

                        # the gdal data stored on the Noaa server runs from longitude 0 to longitude
                        # 360, putting the great ocean at 180 degrees in the center. With the
                        # shiftgrid function we let the longitude run from -180 to 180, so we have
                        # 0 in the center
                        data.values, data.coords["longitude"] = shiftgrid(int(180), data.values,
                                                                          data.coords[
                                                                              "longitude"].values,
                                                                          start=False,
                                                                          cyclic=int(360))

                        # set all the 9999 values to nan so it is masked
                        data.values = data.where(data < 9999)

                        logger.debug("ATTR: {}".format(data.attrs))

                        # append the current 2D data field for this quantity and forecast time to
                        # its own list
                        try:
                            fc_sea_states[new_key_name].append(data)
                        except KeyError:
                            # if it fails to append we create a new field to start the data
                            # collection of all the forecasts
                            fc_sea_states[new_key_name] = list([data])

                    # end of loop over the quantities.
                    # Now store the time of this record
                    fc_times.append(forecast_time)
                    logger.debug(
                        "GeoTransform at {} {} fields: {}".format(forecast_time, len(fc_sea_states),
                                                                  geo_transform))

                    if args.progressbar:
                        PB_WIDGETS[-1] = progress_bar_message(cnt, max_files)
                        progress.update(cnt)
                        sys.stdout.flush()
                        sys.stderr.flush()

                if args.progressbar:
                    progress.finish()

                if not fc_sea_states:
                    logger.debug("No sea states were loaded. Continue to the next time step")
                    continue

                # end of the loop over all the forecast time. We have a dictionary with a key for
                # each quantity carrying a list of time steps. Glue the time steps together into
                # 3D data sets (longitude, latitude, time)
                sea_state_collection = OrderedDict()
                encoding_dict = OrderedDict()
                for quantity, data_time_list in fc_sea_states.items():
                    logger.info("Combining the time steps of {}".format(quantity))
                    data_array = xr.concat(data_time_list, dim="time")
                    # turn the forecast time into a list of pandas time (Timedelta)
                    data_array.coords["time"] = [pd.Timedelta(tt) for tt in fc_times]
                    # add global statistics to the attributes
                    data_array.attrs["min"] = data_array.min().values
                    data_array.attrs["max"] = data_array.max().values
                    data_array.attrs["mean"] = data_array.mean().values
                    data_array.attrs["std"] = data_array.std().values
                    logger.debug("ATTR : {}".format(data_array.attrs))

                    sea_state_collection[quantity] = data_array
                    # create a dictionary with the encodings set true for all variables. The
                    # encodings are used later to be able to zip the data when writing to file
                    encoding_dict[quantity] = dict(zlib=True)

                # combine all 3D quantities data array in one single data set and store to file
                logger.info("Turn in one big collection")
                sea_states = xr.Dataset(sea_state_collection, compat="equals")

                # we have stored the first date time above, set this as the global attribute
                sea_states.attrs["start_date_time"] = \
                    nc.date2num(pd.to_datetime(first_time_stamp),
                                units="hours since 0001-01-01 00:00:00", calendar="gregorian")

                logger.debug("\n".format(sea_states))
                logger.debug(type(sea_states))
                logger.debug("keys:\n{}".format(list(sea_states.keys())))
                logger.debug("vars:\n{}".format(sea_states.data_vars))
                logger.debug("coor:\n{}".format(sea_states.coords))

                if (args.write_selection or settings["selection"][
                    "write_selection"]) and not args.test:
                    # create a selection of the coordinates.
                    llcrnrlat = sea_states.coords["latitude"].min()
                    urcrnrlat = sea_states.coords["latitude"].max()
                    llcrnrlon = sea_states.coords["longitude"].min()
                    urcrnrlon = sea_states.coords["longitude"].max()
                    llcrnrlat = set_value_if_valid(llcrnrlat, settings["selection"][
                        "low_left_corner_latitude"])
                    urcrnrlat = set_value_if_valid(urcrnrlat, settings["selection"][
                        "up_right_corner_latitude"])
                    llcrnrlon = set_value_if_valid(llcrnrlon, settings["selection"][
                        "low_left_corner_longitude"])
                    urcrnrlon = set_value_if_valid(urcrnrlon, settings["selection"][
                        "up_right_corner_longitude"])

                    logger.info(
                        "Creating selection of data latitude {} ~ {} longitude {} ~ {}".format(
                            llcrnrlat, urcrnrlat, llcrnrlon, urcrnrlon))

                    # note that the latitude are stored from north to south, so select from ur to ll
                    sea_states_selection = sea_states.loc[dict(
                        latitude=slice(urcrnrlat, llcrnrlat),
                        longitude=slice(llcrnrlon, urcrnrlon)
                    )]

                    # update the global statistics for the selection
                    for quant_name, data_array in sea_states_selection.data_vars.items():
                        data_array.attrs["min"] = data_array.min().values
                        data_array.attrs["max"] = data_array.max().values
                        data_array.attrs["mean"] = data_array.mean().values
                        data_array.attrs["std"] = data_array.std().values

                else:
                    sea_states_selection = None

                if nowcast_sea_states is not None:
                    # in this section we are going to append to the global time data base
                    logger.info(
                        "START netcdf database section with {}".format(
                            list(nowcast_sea_states.dimensions.keys())))
                    if "latitude" not in list(nowcast_sea_states.dimensions.keys()):

                        logger.debug(
                            "Initialising the nowcast database with latitude/longitude coordinates")
                        # we did not initialise the the latitude yet so we started with a new data
                        # base. Create the coordinate axis latitude and longitude based on the grib
                        # file dimensions

                        nowcast_sea_states.createDimension("latitude",
                                                           sea_states.coords["latitude"].size)
                        nowcast_sea_states.createDimension("longitude",
                                                           sea_states.coords["longitude"].size)

                        longitudes = nowcast_sea_states.createVariable("longitude", np.float32,
                                                                       ('longitude',),
                                                                       zlib=args.compress_data)
                        latitudes = nowcast_sea_states.createVariable("latitude", np.float32,
                                                                      ('latitude',),
                                                                      zlib=args.compress_data)

                        longitudes.units = "degrees_east"
                        latitudes.units = "degrees_north"

                        # set the values based on the values of the grib files
                        longitudes[:] = sea_states.coords["longitude"].values
                        latitudes[:] = sea_states.coords["latitude"].values

                        # add a variable per quantity as found in the grib file
                        variables = OrderedDict()
                        for quant_name, data_array in sea_states.data_vars.items():
                            variables[quant_name] = \
                                nowcast_sea_states.createVariable(quant_name, np.float32,
                                                                  ("time", "latitude", "longitude"),
                                                                  zlib=args.compress_data)
                            variables[quant_name].units = data_array.attrs["units"]

                        # dump all the variable names to screen
                        for varname, var in nowcast_sea_states.variables.items():
                            logger.debug("{} : type = {} dim = {} shape = {}"
                                         "".format(varname, var.dtype, var.dimensions, var.shape))

                        nowcast_sea_states.description = "Sea state quantities over time"
                        nowcast_sea_states.history = "Created " + time.ctime(time.time())
                        nowcast_sea_states.source = "ftp_to_netcdf python script"
                    else:
                        logger.debug("Database was already initialised")

                    # we have a data base now with all the variables and coordinates in place
                    logger.info("Appending to {}".format(args.netcdf_global_output_file_name))
                    logger.debug(nowcast_sea_states.dimensions)
                    logger.debug(list(nowcast_sea_states.dimensions.keys()))
                    try:
                        logger.debug(nowcast_sea_states.history)
                    except AttributeError:
                        logger.debug("No history attribute")
                    try:
                        logger.debug(nowcast_sea_states.source)
                    except AttributeError:
                        logger.debug("No source attribute")

                    # loop over the forecast time
                    for fc_tme in sea_states.coords["time"]:
                        # time stamp is the pandas time stamp of the start of the forecast
                        # fc_tme is the datetime for the coordinates of the current forecast. To
                        # get the real value, use values, then you can add it to time stamp.
                        time_stamp_total = time_stamp + fc_tme.values
                        # convert the total time to the datetime format, which can be converted to
                        # number of hours since 1-1-000
                        time_stamp_total_date = pd.to_datetime(time_stamp_total)
                        hours_since = nc.date2num(time_stamp_total_date, units=nowcast_times.units,
                                                  calendar=nowcast_times.calendar)
                        logger.debug("time_stamp= {} total {}  ({}) hours_since: {}"
                                     "".format(time_stamp, time_stamp_total, type(time_stamp_total),
                                               hours_since))
                        logger.debug("Check time: {}".format(
                            nc.num2date(hours_since, units=nowcast_times.units,
                                        calendar=nowcast_times.calendar)))

                        last_time_index = nowcast_times.shape[0] - 1
                        if last_time_index >= 0:
                            last_time_value = nowcast_times[last_time_index - 1]
                        else:
                            last_time_value = None

                        next_time_index = last_time_index + 1
                        logger.debug(
                            "TIME index/val {} / {}".format(last_time_index, last_time_value))

                        # add the new time to the time array
                        nowcast_times[next_time_index] = hours_since

                        # loop over the variables of the current loaded forecast data sets
                        for quant_name, data_array in sea_states.data_vars.items():
                            # make a reference to the nowcast data base of the same variable
                            nowcast_variable = nowcast_sea_states.variables[quant_name]
                            logger.debug("SHAPE nowcast var {} : {}".format(quant_name,
                                                                            nowcast_variable.shape))

                            # copy the current data quantity at the forecast time fc_tme to the
                            # nowcast database
                            nowcast_variable[next_time_index, :, :] = data_array.loc[
                                dict(time=fc_tme)].values
                            logger.debug("{} : {}".format(quant_name,
                                                          data_array.loc[dict(time=fc_tme)].mean()))
                else:
                    # this section writes a netcdf file of the collection of forecast time.
                    message = "Exporting to {}".format(netcdf_out_file)
                    if args.progressbar:
                        print(message)
                    else:
                        logger.info(message)
                    sea_states.to_netcdf(netcdf_out_file, encoding=encoding_dict, format="NETCDF4")

                    if sea_states_selection is not None:
                        lat1 = "m{}".format(-llcrnrlat) if (llcrnrlat < 0) else "p{}".format(
                            llcrnrlat)
                        lat2 = "m{}".format(-urcrnrlat) if (urcrnrlat < 0) else "p{}".format(
                            urcrnrlat)
                        lon1 = "m{}".format(-llcrnrlon) if (llcrnrlon < 0) else "p{}".format(
                            llcrnrlon)
                        lon2 = "m{}".format(-urcrnrlon) if (urcrnrlon < 0) else "p{}".format(
                            urcrnrlon)
                        netcdf_out_file_selection = "{}_lat_{}_{}_lon_{}_{}.nc".format(
                            out_base_name,
                            lat1, lat2, lon1, lon2)
                        message = "Exporting to {}".format(netcdf_out_file_selection)
                        if args.progressbar:
                            print(message)
                        else:
                            logger.info(message)
                        sea_states_selection.to_netcdf(netcdf_out_file_selection,
                                                       encoding=encoding_dict,
                                                       format="NETCDF4")

                logger.info("Done")
            else:
                if nowcast_sea_states is None:
                    # end of import of all grib file beloning to this date and time
                    logger.info(
                        "Netcfd file '{}' already exists. Skipping ".format(netcdf_out_file))

        if ftp_side is not None:
            # we have read a data file, so move one directory up for the next one
            ftp_side.cwd("..")

    # end of the loop over dates, times, and forecase times.
    if nowcast_sea_states is not None:
        # close the netcdf file
        logger.info("Closing netcdf file {}  ".format(args.netcdf_global_output_file_name))
        nowcast_sea_states.close()

    if args.internet:
        ftp_side.close()


def _run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == '__main__':
    _run()
