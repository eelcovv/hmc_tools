=========
Changelog
=========

Version 0.1.8
=============
- Create RouteInterpolate class for better readability of the route_interpolate script
- Added some more doc strings and a example

Version 0.1.7
=============
- Update to Python 3.6
- Update to pyscaffold 3.0

Version 0.1.6
=============
- Some bug fixes on the download of the local grib files

Version 0.1.5
=============

- New feature to replace 0,0 gps values with interpolated values
- Remove spurious speeds by back padding all erroneous rows
- Added date/time limits to the netcdf manipulate

