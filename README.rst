=========
hmc_tools
=========


A package with a collection of command line utilities to perform several tasks


Description
===========

The following command line utilities are provide

* *ffmpeg_movie*: front-end to the 'ffmpeg.exe' utility (must be installed and present in path) for
  a more user-friendly creation of movies from a series of images
* *ftp_to_netcdf*: download a series of Grib (gridded binary)  files from the NOAA server (or our
  local storage) and turn into a single netcdf file
* *route_interpolate*: interpolate the sea states stored in netcdf file as created by
  *ftp_to_netcdf* and interpolate it on a series of latitude/longitude coordinates stored in an
  excel or kml file
* *svg_objects_export*: read an Inkscape drawing and export all the elements


Note
====

This project has been set up using PyScaffold 3.0. For details and usage
information on PyScaffold see http://pyscaffold.readthedocs.org/.
